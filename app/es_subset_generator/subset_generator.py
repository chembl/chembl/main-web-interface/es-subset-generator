"""
Module that handles the creation of subset indexes
"""
import hashlib
import base64
import time

from app.config import RUN_CONFIG
from app.es_connection import ES
from app.es_subset_generator import mappings
from app.tasks import task_manager
from app.tasks.models import task_models
from app import app_logging


def prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list, test_run=False):
    """
    Creates the subset index to be filled with the items, then creates the task to fill it
    :param origin_index: superset index name
    :param items_ids: ids ot use to fill the subset index
    :param fields_list: fields from which to copy the mapping into the destination index
    :param test_run: If true, tells me to mark the task as test so it can be easily cleaned up
    :return: the id of the task created
    """
    task_exists, task_id = review_existing_task(origin_index, items_ids, fields_list)
    if task_exists:
        return task_id

    subset_index_name, ids_hash, origin_fields_hash = create_subset_index(origin_index, items_ids, fields_list)
    mappings.put_mapping_into_subset_index(origin_index, subset_index_name, fields_list)
    task_id = task_manager.create_and_save_task(
        origin_index,
        subset_index_name,
        items_ids,
        ids_hash,
        fields_list,
        origin_fields_hash,
        test_run
    )

    return task_id


def review_existing_task(origin_index, items_ids, fields_list):
    """
    Checks if the task exists already. if the task exists and its status is not ERROR, it returns True and the ID
    If the task exists and is in error state, it changes its status to QUEUED so it an be run again
    :param origin_index: superset index name
    :param items_ids: ids ot use to fill the subset index
    :param fields_list: fields from which to copy the mapping into the destination index
    :return: True,task_id, if the task exists. False,None if does not exist
    """

    ids_hash = generate_ids_hash(items_ids)
    origin_fields_hash = generate_ids_hash(fields_list)
    subset_index_name = create_index_name(origin_index, ids_hash, origin_fields_hash)

    try:

        existing_task = task_models.search_task_by_params(ids_hash, origin_fields_hash, subset_index_name)
        if existing_task.status == task_models.TaskStatuses.ERROR:
            task_models.set_task_state_to_queued(existing_task)
        return True, existing_task.id

    except task_models.TaskNotFoundError:

        return False, None


def create_subset_index(origin_index, items_ids, fields_list):
    """
    Creates an index based on the origin index, produces an index name with help of the ids provided
    :param origin_index: base index for the creation of the subset index
    :param items_ids: ids to use
    :param fields_list: fields for which to copy the mapping into the destination index. The mappings are not created
    by this function, but the origin fields are used for the id.
    :return: the id of the index created, the ids hash and the origin fields hash to save them in the db.
    """
    ids_hash = generate_ids_hash(items_ids)
    origin_fields_hash = generate_ids_hash(fields_list)
    subset_index_name = create_index_name(origin_index, ids_hash, origin_fields_hash)

    if check_if_index_exists(subset_index_name):
        # If index already exists, delete it to avoid inconsistencies
        ES.indices.delete(index=subset_index_name)

    ES.indices.create(index=subset_index_name)

    return subset_index_name, ids_hash, origin_fields_hash


def create_index_name(origin_index, ids_hash, origin_fields_hash):
    """
    :param origin_index: base index for the creation of the subset index
    :param ids_hash: hash of the ids used to create the subset index
    :param origin_fields_hash: hash of the fields included in the subset index
    :return: a subset index name based on the parameters given
    """

    subset_index_prefix = RUN_CONFIG.get('subset_index_prefix')
    subset_index_name = f'{subset_index_prefix}_{origin_index}_{ids_hash}_{origin_fields_hash}'.lower()
    return subset_index_name


def check_if_index_exists(index_name, retries=0, expected_value=False):
    """
    Checks if the index given as parameter exists
    :param index_name: name of the index to check
    :param retries: number of retries for an expected value. If > 0 will every second the times indicated until
    the expected value is obtained.
    :param expected_value: expected value for the existence of the index: True or False
    :return: True if exists, false otherwise
    """
    if retries == 0:
        return ES.indices.exists(index=index_name)

    app_logging.debug(f'Going to check existence of index {index_name} with {retries} retries. '
                      f'Expected value for existence is {expected_value}')
    current_num_retries = 0
    index_exists = None
    expected_value_is_met = index_exists == expected_value
    while current_num_retries < retries and not expected_value_is_met:

        index_exists = ES.indices.exists(index=index_name)
        expected_value_is_met = index_exists == expected_value
        app_logging.debug(f'index_exists: {index_exists}. Retry number {current_num_retries}')

        current_num_retries += 1
        time.sleep(1)

    return index_exists


def delete_all_test_indexes():
    """
    Deletes from ES the test indexes
    """
    index_name = f'{RUN_CONFIG.get("subset_index_prefix")}*'
    print(f'Deleting test indexes! Indices with wildcard {index_name} will be deleted')
    ES.indices.delete(index=index_name)


def generate_ids_hash(items_ids):
    """
    :param items_ids: ids to use to generate hash
    :return: hash from the ids entered
    """

    sorted_ids = sorted(items_ids)
    sorted_ids_str = ','.join(sorted_ids)
    sorted_ids_digest = hashlib.sha256(sorted_ids_str.encode()).digest()
    base64_search_params_digest = base64.b64encode(sorted_ids_digest).decode('utf-8'). \
        replace('/', '_').replace('+', '-')

    return base64_search_params_digest
