"""
Module to test the creation the subset indexes
"""
# pylint: disable=no-self-use
import unittest

from app.es_subset_generator import subset_generator
from app import create_app
from app.tasks.models import task_models


class TestSubsetIndexGeneration(unittest.TestCase):
    """
        Class to test the generation of subset indexes
    """

    def setUp(self):
        subset_generator.delete_all_test_indexes()

    def tearDown(self):
        subset_generator.delete_all_test_indexes()

    def test_it_creates_a_subset_index(self):
        """
        Test that it can create a subset index with the mapping from the origin index
        """
        origin_index = 'chembl_molecule'
        items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
        fields_list = [
            'molecule_properties.hba',
            'molecule_synonyms',
            'molecule_properties.psa',
            '_metadata.drug.drug_data.prodrug',
            'molecule_properties.molecular_species',
            'helm_notation'
        ]

        index_properties = subset_generator.create_subset_index(
            origin_index,
            items_ids,
            fields_list
        )

        subset_index_name = index_properties[0]

        index_exists = subset_generator.check_if_index_exists(subset_index_name)
        self.assertTrue(index_exists, msg=f'The subset index {subset_index_name} was not created!')


class TestSubsetIndexTaskGeneration(unittest.TestCase):
    """
        Class to test the preparation of the subset index and the generation of the task
    """

    def setUp(self):
        subset_generator.delete_all_test_indexes()
        self.flask_app = create_app()
        self.client = self.flask_app.test_client()
        with self.flask_app.app_context():
            task_models.delete_all_tasks()

    def tearDown(self):
        subset_generator.delete_all_test_indexes()
        with self.flask_app.app_context():
            task_models.delete_all_tasks()

    def test_it_prepares_the_subset_index_and_the_task_to_fill_it(self):
        """
        Tests that it prepares the subset index and the task to fit it
        """

        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
            fields_list = [
                'molecule_properties.hba',
                'molecule_synonyms',
                'molecule_properties.psa',
                '_metadata.drug.drug_data.prodrug',
                'molecule_properties.molecular_species',
                'helm_notation'
            ]

            task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)

            task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()

            origin_index_got = task_got.origin_index
            self.assertEqual(origin_index, origin_index_got, msg=f'The origin index was not saved correctly!')
            items_ids_got = task_got.get_uncompressed_ids()
            self.assertEqual(items_ids, items_ids_got, msg='The items ids was not saved correctly!')

    def test_it_does_not_create_the_same_subset_task_twice(self):
        """
        Tests that if a subset creation task with the same ids and same fields is being processed or finished, it does
        not do the preparation process again. It just returns the id of the task that already exists
        """
        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
            fields_list = [
                'molecule_properties.hba',
                'molecule_synonyms',
                'molecule_properties.psa',
                '_metadata.drug.drug_data.prodrug',
                'molecule_properties.molecular_species',
                'helm_notation'
            ]

            preexisting_task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids,
                                                                                         fields_list)

            # now I try to create another task with the same parameters
            new_task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)

            self.assertEqual(preexisting_task_id, new_task_id, msg='The task returned should be exactly the same!')

    def test_it_recreates_the_task_when_there_was_an_error(self):
        """
        Tests that when there is a task that errored, it is restarted when submitted again.
        """
        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
            fields_list = [
                'molecule_properties.hba',
                'molecule_synonyms',
                'molecule_properties.psa',
                '_metadata.drug.drug_data.prodrug',
                'molecule_properties.molecular_species',
                'helm_notation'
            ]

            errored_task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids,
                                                                                     fields_list)
            errored_task = task_models.get_task_by_id(errored_task_id)
            task_models.set_task_state_to_error(errored_task)

            # now I try to create another task with the same parameters
            new_task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)
            self.assertEqual(errored_task_id, new_task_id, msg='The task returned should be exactly the same!')

            new_task_status_got = task_models.get_task_by_id(new_task_id).status
            self.assertEqual(new_task_status_got, task_models.TaskStatuses.QUEUED)
