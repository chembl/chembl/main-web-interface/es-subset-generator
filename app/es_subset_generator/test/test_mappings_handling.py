"""
Module to test the handling of mappings for the subset indexes
"""
# pylint: disable=no-self-use
import unittest
import json

from app.es_subset_generator import mappings
from app.es_subset_generator import subset_generator


class TestMappingsHandling(unittest.TestCase):
    """
    Class to test the handling of mappings
    """

    def test_parses_mapping_from_es_for_a_single_property(self):
        """
        Tests that given a mapping from ES it produces a mapping to put to the subset index
        """
        sample_mapping_path = 'app/es_subset_generator/test/data/sample_mapping_1.json'

        with open(sample_mapping_path, 'rt') as sample_mapping_file:
            mapping_from_base_index = json.load(sample_mapping_file)
            source = ['pref_name']
            mappings_to_apply_got = mappings.parse_mapping_from_es_response(mapping_from_base_index, source)

            mappings_to_apply_must_be = {
                "properties": {
                    'pref_name': {
                        'type': 'keyword'
                    },
                }
            }

            self.assertEqual(mappings_to_apply_got, mappings_to_apply_must_be,
                             msg='The mappings were not produced correctly!')

    def test_parses_mapping_from_es_for_a_multiple_properties(self):
        """
        Tests that given a mapping from ES it produces a mapping to put to the subset index for multiple properties
        """
        sample_mapping_path = 'app/es_subset_generator/test/data/sample_mapping_1.json'

        with open(sample_mapping_path, 'rt') as sample_mapping_file:
            mapping_from_base_index = json.load(sample_mapping_file)
            source = [
                'molecule_properties.hba',
                'molecule_synonyms',
                'molecule_properties.psa',
                '_metadata.drug.drug_data.prodrug',
                'molecule_properties.molecular_species',
                'helm_notation'
            ]

            mappings_to_apply_got = mappings.parse_mapping_from_es_response(mapping_from_base_index, source)

            mappings_to_apply_must_be_path = 'app/es_subset_generator/test/data/mappings_to_apply_must_be_1.json'

            with open(mappings_to_apply_must_be_path, 'rt') as mappings_to_apply_must_be_file:
                mappings_to_apply_must_be = json.load(mappings_to_apply_must_be_file)

                self.assertEqual(mappings_to_apply_got, mappings_to_apply_must_be,
                                 msg='The mappings were not produced correctly!')

    def test_locates_source_config_of_property(self):
        """
        Test that it locates the config of a property
        """
        sample_mapping_path = 'app/es_subset_generator/test/data/sample_mapping_1.json'

        with open(sample_mapping_path, 'rt') as sample_mapping_file:
            mapping_from_base_index = json.load(sample_mapping_file)
            property_path = 'molecule_properties.full_molformula'

            source_config_got = mappings.locate_source_config(mapping_from_base_index, property_path)

            source_config_must_be = {
                "type": "keyword",
                "fields": {
                    "alphanumeric_lowercase_keyword": {
                        "type": "text",
                        "term_vector": "with_positions_offsets",
                        "analyzer": "alphanumeric_lowercase_keyword"
                    },
                    "keyword": {
                        "type": "keyword"
                    }
                }
            }

            self.assertEqual(source_config_got, source_config_must_be,
                             msg=f'The source config was not obtained correctly!')

    def test_raises_error_when_source_property_does_not_exist(self):
        """
        Tests that when locating the config of a property that does not exist it raises an error
        """
        sample_mapping_path = 'app/es_subset_generator/test/data/sample_mapping_1.json'

        with open(sample_mapping_path, 'rt') as sample_mapping_file:
            with self.assertRaises(mappings.MappingsHandlingError,
                                   msg=f'It should have raised an error for a non existing property'):
                mapping_from_base_index = json.load(sample_mapping_file)
                property_path = 'does_not_exist'

                mappings.locate_source_config(mapping_from_base_index, property_path)

    def test_gets_property_mapping_from_source_config(self):
        """
        Tests that given a property configuration from a mapping, it builds a the mapping to apply for the property
        in ES
        """

        sample_mapping_path = 'app/es_subset_generator/test/data/sample_mapping_1.json'

        with open(sample_mapping_path, 'rt') as sample_mapping_file:
            mapping_from_base_index = json.load(sample_mapping_file)
            property_path = 'molecule_properties.full_molformula'

            mapping_to_apply_got = mappings.get_mapping_to_apply_in_subset_index(mapping_from_base_index, property_path)
            mapping_to_apply_must_be = {
                "type": "keyword"
            }

            self.assertEqual(mapping_to_apply_got, mapping_to_apply_must_be,
                             msg=f'The mapping was not generated correctly!')

    def test_gets_property_mapping_for_a_nested_property(self):
        """
        Gets the mapping of a property when the property is nested
        """

        sample_mapping_path = 'app/es_subset_generator/test/data/sample_mapping_1.json'

        with open(sample_mapping_path, 'rt') as sample_mapping_file:
            mapping_from_base_index = json.load(sample_mapping_file)
            property_path = 'molecule_synonyms'

            mapping_to_apply_got = mappings.get_mapping_to_apply_in_subset_index(mapping_from_base_index, property_path)

            mapping_to_apply_must_be = {
                'properties': {
                    'molecule_synonym': {
                        'type': 'keyword'
                    },
                    'syn_type': {
                        'type': 'keyword'
                    },
                    'synonyms': {
                        'type': 'keyword'
                    }
                }
            }

            self.assertEqual(mapping_to_apply_got, mapping_to_apply_must_be,
                             msg=f'The mapping was not generated correctly!')


# ----------------------------------------------------------------------------------------------------------------------
# Putting maping to newly created index
# ----------------------------------------------------------------------------------------------------------------------

class TestMappingsPutting(unittest.TestCase):
    """
    Class to test the handling of mappings
    """

    def setUp(self):
        subset_generator.delete_all_test_indexes()

    def tearDown(self):
        subset_generator.delete_all_test_indexes()

    def test_puts_mapping_new_index(self):
        """
        Tests that it puts the correct mapping after creating a subset index
        """
        origin_index = 'chembl_molecule'
        items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
        origin_fields = [
            'molecule_properties.hba',
            'molecule_synonyms',
            'molecule_properties.psa',
            '_metadata.drug.drug_data.prodrug',
            'molecule_properties.molecular_species',
            'helm_notation'
        ]

        index_properties = subset_generator.create_subset_index(
            origin_index,
            items_ids,
            origin_fields
        )

        subset_index_name = index_properties[0]

        properties_list = [
            'molecule_properties.hba',
            'molecule_synonyms',
            'molecule_properties.psa',
            '_metadata.drug.drug_data.prodrug',
            'molecule_properties.molecular_species',
            'helm_notation'
        ]

        mappings.put_mapping_into_subset_index(origin_index, subset_index_name, properties_list)
        mapping_must_be = mappings.get_mapping_to_apply_to_subset_index(origin_index, properties_list)

        raw_mapping_got = mappings.get_index_mapping(subset_index_name)
        mapping_got = raw_mapping_got[list(raw_mapping_got.keys())[0]]['mappings']

        self.assertEqual(mapping_got, mapping_must_be, msg='The mapping was not put correctly in the subset index!')
