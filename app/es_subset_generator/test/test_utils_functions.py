"""
Module to test utils functions
"""
import unittest

from app import utils


class TestUtilFunctions(unittest.TestCase):
    """
    Class to test utils function
    """

    def test_gets_list_chunks(self):
        """
        Tests it returns chunks from a list
        """
        list_size = 100
        original_list = list(range(0, list_size))

        for chunk_size in range(1, list_size + 10):
            chunks_obtained = list(utils.get_list_chunks(chunk_size, original_list))
            total_size_obtained = sum([len(chunk) for chunk in chunks_obtained])
            self.assertTrue(total_size_obtained == list_size,
                            msg='The total size of the chunks do not match the size of the original list!')

            items_found = set()
            for item in original_list:
                for chunk in chunks_obtained:
                    if item in chunk:
                        items_found.add(item)

            for item in original_list:
                self.assertIn(item, items_found, msg=f'The item {item} is missing in the chunks!')


    def test_gets_list_chunks_when_chunk_size_is_bigger_than_list(self):
        """
        Test that returns one chunk when the chunk size is bigger than the list
        """
        list_size = 100
        original_list = list(range(0, list_size))

        chunk_size = 200
        chunks_obtained = list(utils.get_list_chunks(chunk_size, original_list))
        num_chunks = len(chunks_obtained)
        self.assertEqual(num_chunks, 1, msg='Only one chunk must have been produced!')


    def test_list_chunk_fails_with_nonsense_numbers(self):
        """
        Tests that the chunks function fails when nonsense numbers are given (negative and zero)
        """

        list_size = 100
        original_list = list(range(0, list_size))

        with self.assertRaises(utils.ChunkSizeNonSenseError,
                               msg='It should raise an error with a nonsense chunk size!'):
            utils.get_list_chunks(0, original_list)
            utils.get_list_chunks(-1, original_list)

    def test_list_chunk_works_with_empty_list(self):
        """
        test chunks function works with an empty list. It should create an empty chunk list
        """
        list_size = 0
        original_list = list(range(0, list_size))

        chunks_obtained = list(utils.get_list_chunks(10, original_list))
        self.assertEqual(chunks_obtained, [], msg=f'No chunks must have been produced!')
