"""
Module with functions to generate queries for the reindexing
"""


class QueriesHelper(Exception):
    """
    Class for errors in this module
    """


def get_list_query_for_index(index_name, ids_list):
    """
    :param index_name: index name for which to generate the quey
    :param ids_list: list of ids for which to generate the query
    :return: the query to use for the list passed as parameter
    """
    if index_name == 'chembl_molecule':
        return get_query_for_chembl_molecule(ids_list)
    if index_name == 'chembl_target':
        return get_query_for_chembl_target(ids_list)
    raise QueriesHelper(f'There is no query configured for the index {index_name}!')


def get_query_for_chembl_molecule(ids_list):
    """
    :param ids_list: list of ids for which to generate the query
    :return: the query to use for the chembl_molecule index
    """
    query = {
        "bool": {
            "filter": {
                "terms": {
                    "molecule_chembl_id": ids_list
                }
            }
        }
    }
    return query


def get_query_for_chembl_target(ids_list):
    """
    :param ids_list: list of ids for which to generate the query
    :return: the query to use for the chembl_molecule index
    """
    query = {
        "bool": {
            "filter": {
                "terms": {
                    "target_chembl_id": ids_list
                }
            }
        }
    }
    return query
