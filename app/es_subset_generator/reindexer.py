"""
Module with the functions to do the reindexation to create subsets
"""
# pylint: disable=too-many-locals,broad-except
import multiprocessing
import random
from concurrent import futures
import traceback
import time

from elasticsearch.helpers import scan, bulk

from app import utils
from app.config import RUN_CONFIG
from app.es_connection import ES
from app import app_logging
from app.es_subset_generator import queries_helper


class SubsetIndexIncompleteError(Exception):
    """
    Class for errors related to the subset index being incomplete
    """


def do_reindexing(origin_index, destination_index, items_ids, origin_fields_list, progress_function):
    """
    :param origin_index: superset index from which to take the items
    :param destination_index: name of the index where the subset will be created
    :param items_ids: ids of the items with which to create the subset
    :param origin_fields_list: list of fields to retrieve from the origin index
    :param progress_function: function to call to report the progress
    :return: a dict with the statistics from the task
    """
    start_time = time.time()

    run_params = get_run_params()
    chunk_size = run_params['chunk_size']

    num_items = len(items_ids)
    num_chunks = int(num_items / chunk_size) + 1

    all_chunks = utils.get_list_chunks(chunk_size, items_ids)

    app_logging.debug(f'Starting subset creation from index {origin_index} to {destination_index}'
                      f' with {num_items} items. Chunk size is {chunk_size}. {num_chunks} chunks will be used')

    threads_multiplier = run_params['threads_multiplier']
    total_items_to_process = len(items_ids)
    all_good, items_with_no_match = launch_and_wait_for_threads(all_chunks, total_items_to_process, threads_multiplier,
                                                                origin_fields_list,
                                                                origin_index, destination_index, progress_function)

    if not all_good:
        missing_items = identify_missing_items(destination_index, items_ids)
        missing_items_msg = f'Missing items: {missing_items}'
        progress_function(progress_percentage=None, failed=True, output_log=None, error_log=str(missing_items_msg))

    end_time = time.time()
    time_taken = end_time - start_time

    run_statistics = {
        'time_taken': time_taken,
        'origin_index': origin_index,
        'num_items': num_items,
        'num_chunks': num_chunks,
        'threads_multiplier': threads_multiplier
    }

    return all_good, run_statistics, items_with_no_match


def launch_and_wait_for_threads(all_chunks, total_items_to_process, threads_multiplier, origin_fields_list,
                                origin_index, destination_index, progress_function):
    """
    :param all_chunks: generator of chunks to process from the original ids list
    :param total_items_to_process: total number of items to process to verify the completion
    :param threads_multiplier: multiplier to apply to the cpu count, to determine how many workers to use
    :param origin_fields_list: fields to copy from the origin index
    :param origin_index: superset index where the items come from
    :param destination_index: subset index to fill
    :param progress_function: function to call to report the progress
    :return: True if the everything finished successfully, False otherwise
    """

    with futures.ThreadPoolExecutor(
            max_workers=multiprocessing.cpu_count() * threads_multiplier) as thread_pool_executor:

        future_to_chunk_number = {}
        chunk_number = 0
        num_items_confirmation = 0
        for chunk in all_chunks:
            chunk_number += 1

            num_items_confirmation += len(chunk)
            chunk_reindexer = ChunkReindexer(
                chunk=chunk,
                origin_fields_list=origin_fields_list,
                origin_index=origin_index,
                destination_index=destination_index
            )

            current_task = thread_pool_executor.submit(chunk_reindexer.run)
            future_to_chunk_number[current_task] = chunk_number

        app_logging.debug(f'{num_items_confirmation} items will be processed with {chunk_number} threads')
        all_good, items_with_no_match = wait_for_results_as_completed(future_to_chunk_number, total_items_to_process,
                                                                      progress_function)

    if all_good:
        try:
            final_index_size_must_be = total_items_to_process - len(items_with_no_match)
            progress_function(progress_percentage=None, failed=False,
                              output_log=f'Waiting for index to be ready... '
                                         f'final_index_size_must_be: {final_index_size_must_be}',
                              error_log=None)
            wait_for_index_to_be_ready(destination_index, final_index_size_must_be)
            progress_function(progress_percentage=None, failed=False,
                              output_log=f'Index ready!!! Final index contains {final_index_size_must_be} items',
                              error_log=None)
        except SubsetIndexIncompleteError as error:
            progress_function(progress_percentage=None, failed=True, output_log=None, error_log=str(error))
            all_good = False

    return all_good, items_with_no_match


def wait_for_index_to_be_ready(destination_index, num_items_must_be):
    """
    Waits for the index to report the correct amount of items
    :param destination_index: subset index to check
    :param num_items_must_be: how many items it must have
    :return: when the index reports the desired amount of items
    """
    index_ready = False
    seconds_waited = 0
    timeout = RUN_CONFIG.get('num_items_check_timeout')
    times_ready = 0

    while not index_ready and seconds_waited < timeout:
        body = {
            "track_total_hits": True
        }
        num_results_got = ES.search(index=destination_index, body=body)['hits']['total']['value']
        app_logging.debug(f'Index {destination_index} reports {num_results_got} items, must be {num_items_must_be}')
        if num_results_got == num_items_must_be:
            times_ready += 1
        index_ready = times_ready == 5
        app_logging.debug(f'Index {destination_index} ready: {index_ready}')

        sleep_time = 0.5
        time.sleep(sleep_time)

        seconds_waited += sleep_time

    if not index_ready:
        error_msg = f'The index {destination_index} is incomplete!'
        app_logging.debug(error_msg)
        raise SubsetIndexIncompleteError(error_msg)


def identify_missing_items(destination_index, items_ids):
    """
    Identifies the items that are missing in the destination index when it's already known that the index is incomplete
    :param destination_index: subset index with the items to verify
    :param items_ids: the ids that must contain the destination index
    :return: a set with the missing items in the destination index
    """
    items_ids_must_be_set = set(items_ids)
    items_ids_got_set = set()

    es_scanner = scan(
        ES,
        index=destination_index,
        scroll=u'1m',
        size=1000,
        request_timeout=30,
        query={
            "_source": []
        }
    )

    for doc_i in es_scanner:
        current_id = doc_i['_id']
        items_ids_got_set.add(current_id)

    missing_items = items_ids_must_be_set - items_ids_got_set
    app_logging.debug(f'Missing items on index {destination_index}: {missing_items}')
    return missing_items


def get_run_params():
    """
    :return: a dictionary with the run parameters chosen to run the reindexing,
    the chunk size, and the thread multiplier
    """
    chunk_size_config = RUN_CONFIG.get('chunk_size')
    min_chunk_size = chunk_size_config.get('min_size')
    max_chunk_size = chunk_size_config.get('max_size')
    chunk_size = random.randint(min_chunk_size, max_chunk_size)

    threads_multiplier_config = RUN_CONFIG.get('threads_multiplier')
    min_threads_multiplier = threads_multiplier_config.get('min_multiplier')
    max_threads_multiplier = threads_multiplier_config.get('max_multiplier')
    threads_multiplier = random.randint(min_threads_multiplier, max_threads_multiplier)

    run_params = {
        'chunk_size': chunk_size,
        'threads_multiplier': threads_multiplier
    }

    return run_params


def wait_for_results_as_completed(future_to_chunk_number, total_items_to_process, progress_function):
    """
    :param future_to_chunk_number: structure mapping the future to the chunk number it corresponds to
    :param total_items_to_process: total items to be processed
    :param progress_function: function to call to report the progress
    :return: When all the tasks have finished, True if everything was ok, False if something failed
    """

    total_items_processed = 0
    progress = 0
    all_good = True
    items_with_no_match = set()
    for future in futures.as_completed(future_to_chunk_number):
        chunk_number = future_to_chunk_number[future]

        try:

            num_items_processed, current_items_with_no_match = future.result()
            total_items_processed += num_items_processed
            items_with_no_match |= current_items_with_no_match
            progress = int((total_items_processed / total_items_to_process) * 100)
            output_log = f'chunk {chunk_number} finished successfully. {num_items_processed} processed.' \
                         f' {total_items_processed} until now. {len(current_items_with_no_match)} items in this chunk' \
                         f' with no match.'
            progress_function(progress, failed=False, output_log=output_log, error_log=None)
            app_logging.debug(output_log)

        except Exception:

            all_good = False
            stack_trace = traceback.format_exc()
            error_log = f'There was an error while processing chunk {chunk_number}:\n{stack_trace}'
            progress_function(progress, failed=True, output_log=None, error_log=error_log)
            app_logging.debug(error_log)

    return all_good, items_with_no_match


# ----------------------------------------------------------------------------------------------------------------------
# Chunk reindexer class
# ----------------------------------------------------------------------------------------------------------------------
class ChunkReindexer:
    """
    Class that defines a chunk reindexer
    """

    def __init__(self, chunk, origin_fields_list, origin_index, destination_index):
        """
        creates an instance of the reindexer for a particular chunk
        :param chunk: chunk with ids to process
        :param origin_fields_list: list of fields to get from the origin index
        :param origin_index: index from where to get the items
        :param destination_index: index where to save the items
        """
        self.chunk = chunk
        self.all_ids_set = set(self.chunk)
        self.total_items_to_process = len(self.chunk)
        self.origin_fields_list = origin_fields_list
        self.origin_index = origin_index
        self.destination_index = destination_index

    def get_scanner_for_chunk(self):
        """
        Creates a scanner for this chunk to get the items assigned to it
        :return: the scanner to use to process the chunk
        """
        query = queries_helper.get_list_query_for_index(self.origin_index, self.chunk)

        es_scanner = scan(
            ES,
            index=self.origin_index,
            scroll=u'1m',
            size=1000,
            request_timeout=30,
            query={
                "_source": self.origin_fields_list,
                "query": query
            }
        )

        return es_scanner

    def run(self):
        """
        Runs the reindexation process
        """
        es_scanner = self.get_scanner_for_chunk()

        actions = []
        for doc_i in es_scanner:
            doc_id = doc_i['_id']
            action = {
                '_index': self.destination_index,
                '_id': doc_id,
                '_source': doc_i['_source'],
            }
            actions.append(action)
            self.all_ids_set.remove(doc_id)

        num_items_processed, errors = bulk(ES, actions, refresh='wait_for')
        app_logging.debug(f'Errors: {errors}')
        items_with_no_match = self.all_ids_set
        num_items_processed += len(items_with_no_match)

        return num_items_processed, items_with_no_match
