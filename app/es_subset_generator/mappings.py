"""
Module that helps to handle the mappings for the subset indexes
"""
from app import utils
from app.es_connection import ES


class MappingsHandlingError(Exception):
    """
    Class to define an error when handling the mappings
    """


def put_mapping_into_subset_index(origin_index_name, subset_index_name, properties_list):
    """
    Given a subset index and an origin index, puts a mapping based on the origin index to the subset index
    :param origin_index_name: name of the index from which the data comes
    :param subset_index_name: name of the index where the subset will be created
    :param properties_list: list of properties for which to copy the mapping
    """

    mapping_to_apply = get_mapping_to_apply_to_subset_index(origin_index_name, properties_list)
    ES.indices.put_mapping(index=subset_index_name, body=mapping_to_apply)


def get_mapping_to_apply_to_subset_index(base_index, properties_list):
    """
    :param base_index: base index from which to get the mappings
    :param properties_list: list of properties for which to copy the mapping
    :return: the mapping to apply for a subset index based on another index and a source
    """

    raw_mapping_response = ES.indices.get_mapping(index=base_index)
    base_index_mapping = raw_mapping_response[list(raw_mapping_response.keys())[0]]['mappings']
    mapping_to_apply = parse_mapping_from_es_response(base_index_mapping, properties_list)

    return mapping_to_apply


def get_index_mapping(index_name):
    """
    :param index_name: name of the index for which to get the mapping
    :return: the mapping of the index
    """
    return ES.indices.get_mapping(index=index_name)


def parse_mapping_from_es_response(index_mapping, required_source):
    """
    Reads the response of the mapping from ES and produces the mapping to use for the subset index
    :param index_mapping: mapping dict obtained from es
    :param required_source: source (list of properties) to get from the mapping
    :return: the mapping ready to be applied to the subset index
    """
    all_mappings_to_apply = {
        'properties': {}
    }

    for current_property_path in required_source:
        mapping_to_apply = get_mapping_to_apply_in_subset_index(index_mapping, current_property_path)
        add_mapping_to_property_path(mapping_to_apply, all_mappings_to_apply, current_property_path)

    return all_mappings_to_apply


def add_mapping_to_property_path(mapping_to_apply, all_mappings_to_apply, property_path):
    """
    Adds the mapping to all the mappings to apply to the path indicated as parameter
    :param mapping_to_apply: mapping to add to the dict
    :param all_mappings_to_apply: a dict with all the mappings to apply
    :param property_path: path to the property
    """
    prop_path_parts = property_path.split('.')
    current_prop = prop_path_parts[0]

    if len(prop_path_parts) == 1:

        all_mappings_to_apply['properties'][current_prop] = mapping_to_apply

    else:

        container_dict = all_mappings_to_apply['properties'].get(current_prop)

        if container_dict is None:
            all_mappings_to_apply['properties'][current_prop] = {
                'properties': {}
            }

        rest_of_the_path_parts = prop_path_parts[1:]
        rest_of_the_path = '.'.join(rest_of_the_path_parts)
        inside_mappings_to_apply = all_mappings_to_apply['properties'][current_prop]
        add_mapping_to_property_path(mapping_to_apply, inside_mappings_to_apply, rest_of_the_path)


def get_mapping_to_apply_in_subset_index(index_mapping, property_path):
    """
    :param index_mapping:
    :param property_path:
    :return: the mapping to apply in the subset index from the property configuration
    """

    source_property_config = locate_source_config(index_mapping, property_path)
    is_nested = source_property_config.get('properties') is not None

    if not is_nested:
        mapping_to_apply = {
            'type': source_property_config['type']
        }
        return mapping_to_apply

    mapping_to_apply = {
        'properties': {}
    }
    for property_key, property_config in source_property_config.get('properties').items():
        mapping_to_parse = {
            'properties': {
                property_key: property_config
            }
        }

        sub_property_mapping = get_mapping_to_apply_in_subset_index(mapping_to_parse, property_key)
        mapping_to_apply['properties'][property_key] = sub_property_mapping

    return mapping_to_apply


def locate_source_config(index_mapping, property_path):
    """
    :param index_mapping: mapping of the original index
    :param property_path: path of the property to get
    :return: the configuration of the property according to the mapping got
    """
    parsed_property_path = f'properties.{property_path.replace(".", ".properties.")}'
    property_config = utils.get_dict_value(index_mapping, parsed_property_path)

    if property_config is None:
        raise MappingsHandlingError(f'The property {property_path} does not exist.')

    return property_config
