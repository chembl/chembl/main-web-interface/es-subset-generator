"""
Module that handles the connection with elasticsearch
"""
import elasticsearch
from app.config import RUN_CONFIG

ELASTICSEARCH_HOST = RUN_CONFIG.get('elasticsearch').get('host')
ELASTICSEARCH_PORT = RUN_CONFIG.get('elasticsearch').get('port')
ELASTICSEARCH_USERNAME = RUN_CONFIG.get('elasticsearch').get('username')
ELASTICSEARCH_PASSWORD = RUN_CONFIG.get('elasticsearch').get('password')
ELASTICSEARCH_TIMEOUT = RUN_CONFIG.get('elasticsearch').get('timeout', 300)
ELASTICSEARCH_RETRY_ON_TIMEOUT = RUN_CONFIG.get('elasticsearch').get('retry_on_timeout', True)

ES = elasticsearch.Elasticsearch(
    hosts=[ELASTICSEARCH_HOST],
    http_auth=(ELASTICSEARCH_USERNAME, ELASTICSEARCH_PASSWORD),
    retry_on_timeout=ELASTICSEARCH_RETRY_ON_TIMEOUT,
    port=ELASTICSEARCH_PORT,
    timeout=ELASTICSEARCH_TIMEOUT,
)
