"""
Module that handles the generation of tokens for the app
"""
import datetime
from datetime import timezone

import jwt

from app.config import RUN_CONFIG

ADMIN_TOKEN_HOURS_TO_LIVE = 1


def generate_admin_token():
    """
    Generates a token that can be used to be authorised for admin tasks
    :return: JWT token
    """

    token_data = {
        'username': RUN_CONFIG.get('admin_username'),
        'exp': datetime.datetime.now(timezone.utc) + datetime.timedelta(hours=ADMIN_TOKEN_HOURS_TO_LIVE)
    }

    key = RUN_CONFIG.get('server_secret_key')
    token = jwt.encode(token_data, key).decode('UTF-8')

    return token
