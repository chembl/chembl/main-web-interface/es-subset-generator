"""
    Module with the classes related to the task ids
"""
from enum import Enum
import datetime
from datetime import timezone
import zlib

from sqlalchemy import and_
from elasticsearch import exceptions as es_exceptions

from app.db import DB
from app.config import RUN_CONFIG
from app import utils
from app.es_connection import ES
from app import app_logging


# pylint: disable=no-member,too-few-public-methods
class TaskStatuses(Enum):
    """
        Possible statuses of the tasks
    """
    QUEUED = 'QUEUED'
    RUNNING = 'RUNNING'
    ERROR = 'ERROR'
    FINISHED = 'FINISHED'

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


# ----------------------------------------------------------------------------------------------------------------------
# Exceptions
# ----------------------------------------------------------------------------------------------------------------------
class TaskNotFoundError(Exception):
    """Base class for exceptions."""


def get_mr_meeseeks_log_text(text):
    """
    Builds the final log texts for Mr Meeseeks
    :param text: text to format
    """
    return utils.get_timestamped_text(f"I'm Mr. Meeseeks! Look at me! - {text}")


class SubsetIndexTask(DB.Model):
    """
    Class that represents a subset index task in the database.
    """
    id = DB.Column(DB.Integer, primary_key=True, autoincrement=True)
    status = DB.Column(DB.Enum(TaskStatuses), default=TaskStatuses.QUEUED)
    progress = DB.Column(DB.Integer, default=0)
    created_at = DB.Column(DB.DateTime, default=datetime.datetime.now(timezone.utc))
    started_at = DB.Column(DB.DateTime)
    finished_at = DB.Column(DB.DateTime)
    expires_at = DB.Column(DB.DateTime)
    last_mr_meeseeks_report_at = DB.Column(DB.DateTime)
    mr_meeseeks_log = DB.Column(DB.Text, default='')
    mr_meeseeks_error_log = DB.Column(DB.Text, default='')
    origin_index = DB.Column(DB.String(length=240))
    destination_index = DB.Column(DB.String(length=240))
    compressed_ids = DB.Column(DB.LargeBinary())
    num_items_to_process = DB.Column(DB.Integer)
    num_items_with_match = DB.Column(DB.Integer)
    num_items_with_no_match = DB.Column(DB.Integer)
    compressed_ids_with_no_match = DB.Column(DB.LargeBinary())
    origin_fields_str = DB.Column(DB.Text)
    ids_hash = DB.Column(DB.String(length=240))
    origin_fields_hash = DB.Column(DB.String(length=240))
    worker = DB.Column(DB.String(length=240))
    output_log = DB.Column(DB.Text, default='')
    error_log = DB.Column(DB.Text, default='')
    test_run = DB.Column(DB.Boolean, default=False)

    def get_uncompressed_ids(self):
        """
        :return: the uncompressed list of te ids that this task is about
        """
        ids_internal_separator = RUN_CONFIG.get('ids_internal_separator')
        decompressed_ids_text = zlib.decompress(self.compressed_ids).decode()
        items_ids_got = decompressed_ids_text.split(ids_internal_separator)

        return items_ids_got

    def get_uncompressed_ids_with_no_match(self):
        """
        :return: the uncompressed list of te ids that this task is about
        """
        if self.compressed_ids_with_no_match is None:
            return []

        ids_internal_separator = RUN_CONFIG.get('ids_internal_separator')
        decompressed_ids_text = zlib.decompress(self.compressed_ids_with_no_match).decode()
        items_ids_with_no_match = decompressed_ids_text.split(ids_internal_separator)

        return items_ids_with_no_match

    def get_origin_fields_list(self):
        """
        :return: the uncompressed list of te ids that this task is about
        """
        ids_internal_separator = RUN_CONFIG.get('ids_internal_separator')
        origin_fields = self.origin_fields_str.split(ids_internal_separator)

        return origin_fields

    def append_to_output_log(self, output_text):
        """
        Appends the text passed as parameter to the output log
        :param output_text: text to append to the output log
        """

        self.output_log += utils.get_timestamped_text(output_text)

    def append_to_error_log(self, error_text):
        """
        Appends the text passed as parameter to the error log
        :param error_text: text to append to the error log
        """
        self.error_log += utils.get_timestamped_text(error_text)

    def append_to_mr_meeseeks_log(self, log_text):
        """
        Appends the text passed as parameter to the Mr Meeseeks log
        :param log_text: text to append to the log
        """
        self.mr_meeseeks_log += get_mr_meeseeks_log_text(log_text)
        self.last_mr_meeseeks_report_at = datetime.datetime.now(timezone.utc)
        save_task(self)

    def append_to_mr_meeseeks_error_log(self, log_text):
        """
        Appends the text passed as parameter to the Mr Meeseeks error log
        :param log_text: text to append to the error log
        """
        self.mr_meeseeks_error_log += get_mr_meeseeks_log_text(log_text)
        self.last_mr_meeseeks_report_at = datetime.datetime.now(timezone.utc)
        save_task(self)

    def get_iso_created_at(self):
        """
        :return: the created at time in ISO format
        """
        return get_iso_datetime_string(self.created_at)

    def get_iso_started_at(self):
        """
        :return: the started at time in ISO format
        """
        return get_iso_datetime_string(self.started_at)

    def get_iso_finished_at(self):
        """
        :return: the finished at time in ISO format
        """
        return get_iso_datetime_string(self.finished_at)

    def get_iso_expires_at(self):
        """
        :return: the expires at time in ISO format
        """
        return get_iso_datetime_string(self.expires_at)


def get_iso_datetime_string(original_datetime):
    """
    :param original_datetime: datetime that you want to convert
    :return: the date time in ISO format with the timezone included
    """
    if original_datetime is None:
        return None
    return original_datetime.astimezone().isoformat()


# ----------------------------------------------------------------------------------------------------------------------
# Items ids compression
# ----------------------------------------------------------------------------------------------------------------------
def get_compressed_ids(items_ids):
    """
    :param items_ids: ids to copy to the subset index
    :return: a compressed version of the ids.
    """
    ids_internal_separator = RUN_CONFIG.get('ids_internal_separator')

    ids_text = ids_internal_separator.join(items_ids)
    ids_bytes = ids_text.encode()
    compressed_ids = zlib.compress(ids_bytes)
    return compressed_ids


# ----------------------------------------------------------------------------------------------------------------------
# CRUD
# ----------------------------------------------------------------------------------------------------------------------
def delete_all_tasks():
    """
    Deletes all tasks in the database
    """
    SubsetIndexTask.query.filter_by().delete()
    DB.session.commit()


def pop_task_to_do():
    """
    Finds the tasks that are in QUEUED state, changes it to RUNNING stated and returns it
    :return: the task popped, None if there are no tasks to do
    """
    task_to_do = SubsetIndexTask.query.filter_by(status=TaskStatuses.QUEUED).first()
    if task_to_do is None:
        return None
    task_to_do.status = TaskStatuses.RUNNING

    save_task(task_to_do)
    return task_to_do


def save_task(task):
    """
    Saves a task to the database, making sure to commit its current status.
    :param task: task to save.
    """
    DB.session.add(task)
    DB.session.commit()


def search_task_by_params(ids_hash, origin_fields_hash, subset_index_name):
    """
    Searches the task by the parameters given, which are the main parameters of a task
    :param ids_hash: hash of the ids used to create the subset index
    :param origin_fields_hash: hash of the fields included in the subset index
    :param subset_index_name: base index for the creation of the subset index
    :return: the task that matches with the parameters given, raises error if not found
    """
    existence_filter = and_(
        SubsetIndexTask.ids_hash == ids_hash,
        SubsetIndexTask.origin_fields_hash == origin_fields_hash,
        SubsetIndexTask.destination_index == subset_index_name
    )

    searched_task = SubsetIndexTask.query.filter(existence_filter).first()

    if searched_task is None:
        raise TaskNotFoundError(f'No task found with ids_hash {ids_hash}, origin_fields_hash {origin_fields_hash}, '
                                f'subset_index_name {subset_index_name}')

    return searched_task


def get_task_by_id(task_id, force_refresh=False):
    """
    :param task_id: id of the task to search
    :param force_refresh: force a refresh on the object
    :return: the task found with the id. Raises error if not found.
    """
    if force_refresh:
        DB.session.commit()
        DB.session.expire_all()

    task = SubsetIndexTask.query.filter_by(id=task_id).first()

    if task is None:
        raise TaskNotFoundError(f'No task found with id {task_id}')

    return task


def delete_task_by_id(task_id):
    """
    Deletes the task with the id given
    :param task_id: the id of the task to delete
    """
    task = get_task_by_id(task_id)
    delete_task(task)


def delete_task(task):
    """
    Deletes a task from the database, making sure to commit the changes.
    :param task: task to delete.
    """
    DB.session.delete(task)
    DB.session.commit()


def set_task_state_to_queued(task):
    """
    Sets the task state to the QUEUED State and saves it to the database
    :param task: task to update
    """
    task.status = TaskStatuses.QUEUED
    save_task(task)


def set_task_state_to_running(task):
    """
    Sets the task state to the RUNNING State and saves it to the database
    :param task: task to update
    """
    task.status = TaskStatuses.RUNNING
    save_task(task)


def set_task_state_to_error(task):
    """
    Sets the task state to the ERROR State and saves it to the database
    :param task: task to update
    """
    task.status = TaskStatuses.ERROR
    task.finished_at = datetime.datetime.now(timezone.utc)
    task.expires_at = generate_expiration_date_from_now()
    save_task(task)


def set_task_state_to_finished(task):
    """
    Sets the task state to the FINISHED State and saves it to the database
    :param task: task to update
    """
    task.status = TaskStatuses.FINISHED
    task.finished_at = datetime.datetime.now(timezone.utc)
    task.expires_at = generate_expiration_date_from_now()
    save_task(task)


# ----------------------------------------------------------------------------------------------------------------------
# Admin Tasks
# ----------------------------------------------------------------------------------------------------------------------
def delete_test_tasks_and_indexes():
    """
    Deletes the tasks marked as test_run and their corresponding indexes
    :return: the number of tasks deleted
    """
    test_tasks = SubsetIndexTask.query.filter_by(test_run=True)
    tasks_deleted = 0
    for task in test_tasks:
        destination_index = task.destination_index
        try:
            ES.indices.delete(index=destination_index)
        except es_exceptions.NotFoundError:
            pass  # if the index has already been deleted it's fine
        DB.session.delete(task)
        tasks_deleted += 1

    DB.session.commit()
    return tasks_deleted


def delete_all_expired_tasks():
    """
    Deletes all the tasks that have expired
    :return: the number of tasks deleted
    """
    now = datetime.datetime.now(timezone.utc)
    app_logging.debug(f'Going to delete tasks with expiration date before {now}')
    tasks_to_delete = SubsetIndexTask.query.filter(SubsetIndexTask.expires_at < now)
    num_deleted = 0

    for task in tasks_to_delete:
        delete_task(task)
        try:
            ES.indices.delete(index=task.destination_index)
        except es_exceptions.NotFoundError:
            pass  # if the index has already been deleted it's fine
        num_deleted += 1

    return num_deleted


# ----------------------------------------------------------------------------------------------------------------------
# Helpers
# ----------------------------------------------------------------------------------------------------------------------
def generate_expiration_date_from_now():
    """
    :return: an expiration date from now based on the configuration
    """

    now = datetime.datetime.now(timezone.utc)
    delta = datetime.timedelta(hours=RUN_CONFIG.get('task_expiration_hours'))
    expiration_date = now + delta

    return expiration_date
