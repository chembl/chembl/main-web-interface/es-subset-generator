"""
Module to handle the reindexation tasks
"""
# pylint: disable=no-member
from app.tasks.models import task_models
from app.db import DB
from app.config import RUN_CONFIG


def create_and_save_task(origin_index, destination_index, items_ids, ids_hash, origin_fields_list, origin_fields_hash,
                         test_run=False):
    """
    Creates a reindexing task and saves it to the database. It sets up a preeliminary expiration date that must be
    updated when the job finishes or fails
    :param origin_index: source index from which to do the reindexation
    :param destination_index: index to fill up with the items
    :param items_ids: ids to use for the subset generation
    :param ids_hash: hash of the ids to use for the subset
    :param origin_fields_list: list of the properties to get from the origin index
    :param origin_fields_hash: hash of the fields used on the subset
    :param test_run: If true, tells me to mark the task as test so it can be easily cleaned up
    :return: the id of task created
    """
    compressed_ids = task_models.get_compressed_ids(items_ids)

    ids_internal_separator = RUN_CONFIG.get('ids_internal_separator')
    origin_fields_str = ids_internal_separator.join(origin_fields_list)

    initial_expiration_date = task_models.generate_expiration_date_from_now()

    task = task_models.SubsetIndexTask(
        origin_index=origin_index,
        destination_index=destination_index,
        compressed_ids=compressed_ids,
        ids_hash=ids_hash,
        origin_fields_str=origin_fields_str,
        origin_fields_hash=origin_fields_hash,
        num_items_to_process=len(items_ids),
        test_run=test_run,
        expires_at=initial_expiration_date
    )

    DB.session.add(task)
    DB.session.commit()

    return task.id
