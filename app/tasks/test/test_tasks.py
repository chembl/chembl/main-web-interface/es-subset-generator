"""
Module to test the creation of tasks for reindexing
"""
# pylint: disable=no-self-use,too-many-locals
import unittest
from datetime import datetime, timedelta, timezone
import random

from app.tasks import task_manager
from app import create_app
from app.tasks.models import task_models
from app.config import RUN_CONFIG
from app.es_subset_generator import subset_generator


# ----------------------------------------------------------------------------------------------------------------------
# Utils
# ----------------------------------------------------------------------------------------------------------------------
def generate_task_for_expiration_time(expiration_time):
    """
    generates a task with the expiration time given as parameter
    :param expiration_time: time that you want for the task to expire
    :return: the task created
    """
    origin_index = 'chembl_molecule'

    items_ids = [random.choice(['a', 'b', 'c']), random.choice(['d', 'e', 'f']), random.choice(['g', 'h', 'i'])]
    fields_list = ['f1', 'f2', 'f3']
    subset_index_name, ids_hash, origin_fields_hash = subset_generator.create_subset_index(origin_index, items_ids,
                                                                                           fields_list)

    task_id = task_manager.create_and_save_task(
        origin_index,
        subset_index_name,
        items_ids,
        ids_hash,
        fields_list,
        origin_fields_hash
    )

    task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()
    task_got.expires_at = expiration_time
    task_models.save_task(task_got)

    return task_got


class TestMappingsHandling(unittest.TestCase):
    """
    Class to test the handling of mappings
    """

    def setUp(self):
        self.flask_app = create_app()
        self.client = self.flask_app.test_client()
        with self.flask_app.app_context():
            task_models.delete_all_tasks()
        subset_generator.delete_all_test_indexes()

    def tearDown(self):
        with self.flask_app.app_context():
            task_models.delete_all_tasks()
        subset_generator.delete_all_test_indexes()

    def test_creates_task(self):
        """
        Test that creates a task
        """

        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            destination_index = 'chembl_molecule_destination'
            ids_hash = 'ABC'
            origin_fields_hash = 'DEF'
            origin_fields_list = ['A', 'B', 'C', 'D']
            items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']

            task_id = task_manager.create_and_save_task(
                origin_index,
                destination_index,
                items_ids,
                ids_hash,
                origin_fields_list,
                origin_fields_hash
            )

            task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()

            origin_index_got = task_got.origin_index
            self.assertEqual(origin_index, origin_index_got, msg=f'The origin index was not saved correctly!')

            destination_index_got = task_got.destination_index
            self.assertEqual(destination_index, destination_index_got,
                             msg=f'The destination index was not saved correctly!')

            items_ids_got = task_got.get_uncompressed_ids()

            self.assertEqual(items_ids, items_ids_got, msg='The items ids were not saved correctly!')

            now = datetime.now(timezone.utc)
            delta = timedelta(hours=RUN_CONFIG.get('task_expiration_hours'))
            expiration_date_must_be = now + delta
            expiration_date_must_be_timestamp = expiration_date_must_be.timestamp()

            expiration_date_got = task_got.expires_at.replace(tzinfo=timezone.utc)
            expiration_date_got_timestamp = expiration_date_got.timestamp()

            self.assertAlmostEqual(expiration_date_got_timestamp, expiration_date_must_be_timestamp,
                                   msg='The initial expiration date was not calculated correctly', delta=1)

    def test_updates_expiration_time_when_finished(self):
        """
        Tests that when a task finishes, it updates its expiration time
        """

        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            destination_index = 'chembl_molecule_destination'
            ids_hash = 'ABC'
            origin_fields_hash = 'DEF'
            origin_fields_list = ['A', 'B', 'C', 'D']
            items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']

            task_id = task_manager.create_and_save_task(
                origin_index,
                destination_index,
                items_ids,
                ids_hash,
                origin_fields_list,
                origin_fields_hash
            )

            task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()
            task_got.expires_at = None
            task_models.save_task(task_got)
            self.assertIsNone(task_got.expires_at, msg='The expiration date was not erased!')

            task_models.set_task_state_to_finished(task_got)

            now = datetime.now(timezone.utc)
            delta = timedelta(hours=RUN_CONFIG.get('task_expiration_hours'))
            expiration_date_must_be = now + delta
            expiration_date_must_be_timestamp = expiration_date_must_be.timestamp()

            expiration_date_got = task_got.expires_at.replace(tzinfo=timezone.utc)
            expiration_date_got_timestamp = expiration_date_got.timestamp()

            self.assertAlmostEqual(expiration_date_got_timestamp, expiration_date_must_be_timestamp,
                                   msg='The initial expiration date was not calculated correctly', delta=1)

    def test_updates_expiration_time_when_errored(self):
        """
        Tests that when a task errors, it updates its expiration time
        """

        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            destination_index = 'chembl_molecule_destination'
            ids_hash = 'ABC'
            origin_fields_hash = 'DEF'
            origin_fields_list = ['A', 'B', 'C', 'D']
            items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']

            task_id = task_manager.create_and_save_task(
                origin_index,
                destination_index,
                items_ids,
                ids_hash,
                origin_fields_list,
                origin_fields_hash
            )

            task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()
            task_got.expires_at = None
            task_models.save_task(task_got)
            self.assertIsNone(task_got.expires_at, msg='The expiration date was not erased!')

            task_models.set_task_state_to_error(task_got)

            now = datetime.now(timezone.utc)
            delta = timedelta(hours=RUN_CONFIG.get('task_expiration_hours'))
            expiration_date_must_be = now + delta
            expiration_date_must_be_timestamp = expiration_date_must_be.timestamp()

            expiration_date_got = task_got.expires_at.replace(tzinfo=timezone.utc)
            expiration_date_got_timestamp = expiration_date_got.timestamp()

            self.assertAlmostEqual(expiration_date_got_timestamp, expiration_date_must_be_timestamp,
                                   msg='The initial expiration date was not calculated correctly', delta=1)

    def test_deletes_expired_tasks(self):
        """
        Tests that deletes the expired task
        """

        with self.flask_app.app_context():

            expired_time = datetime.now(timezone.utc) - timedelta(days=1)
            non_expired_time = datetime.now(timezone.utc) + timedelta(days=1)

            indexes_that_must_be_deleted = []
            indexes_that_must_not_be_deleted = []
            num_deleted_must_be = 0
            task_ids_that_must_be_deleted = []
            task_ids_that_must_not_be_deleted = []
            for chosen_time in [expired_time, non_expired_time]:

                num_i = 0
                while num_i < 3:
                    num_i += 1
                    task_got = generate_task_for_expiration_time(chosen_time)

                    if chosen_time == expired_time:
                        indexes_that_must_be_deleted.append(task_got.destination_index)
                        num_deleted_must_be += 1
                        task_ids_that_must_be_deleted.append(task_got.id)
                    else:
                        indexes_that_must_not_be_deleted.append(task_got.destination_index)
                        task_ids_that_must_not_be_deleted.append(task_got.id)

            num_deleted_got = task_models.delete_all_expired_tasks()

            self.assertEqual(num_deleted_got, num_deleted_must_be,
                             msg=f'{num_deleted_must_be} tasks must have been deleted. '
                                 f'But {num_deleted_got} were deleted!')

            for task_id in task_ids_that_must_be_deleted:
                with self.assertRaises(task_models.TaskNotFoundError,
                                       msg=f'The task with id {task_id} must have been deleted'):
                    task_models.get_task_by_id(task_id)

            for task_id in task_ids_that_must_not_be_deleted:
                task_not_deleted = task_models.get_task_by_id(task_id)
                self.assertIsNotNone(task_not_deleted, msg=f'The task with id {task_id} must exist')

            for index_name in indexes_that_must_be_deleted:
                index_exists = subset_generator.check_if_index_exists(
                    index_name=index_name,
                    retries=50,
                    expected_value=False
                )
                self.assertFalse(index_exists, msg=f'The index {index_name} must have been deleted but it was not!')

            for index_name in indexes_that_must_not_be_deleted:
                index_exists = subset_generator.check_if_index_exists(
                    index_name=index_name,
                    retries=50,
                    expected_value=True
                )
                self.assertTrue(index_exists, msg=f'The index {index_name} must have not been deleted but it was!')
