"""
Module that provide services for administrative tasks of the system
"""
from app.tasks.models import task_models


def delete_test_tasks_and_indexes():
    """
    Deletes all the test tasks and indexes
    :return: a message (string) with the result of the operation
    """
    num_deleted = task_models.delete_test_tasks_and_indexes()
    return f'Deleted {num_deleted} test tasks and their respective indexes'


def delete_expired_tasks():
    """
    Deletes all the expired tasks and indexes
    :return: a message (string) with the result of the operation
    """
    num_deleted = task_models.delete_all_expired_tasks()
    return f'Deleted {num_deleted} expired tasks and their respective indexes'
