"""
Blueprint for authorisation endpoints for the administration of the system
"""
from flask import Blueprint, jsonify, request, make_response

from app.blueprints.admin.services import authorisation_service

ADMIN_AUTH_BLUEPRINT = Blueprint('admin_auth', __name__)


@ADMIN_AUTH_BLUEPRINT.route('/login', methods=['GET'])
def login():
    """
    logs in the admin user
    :return: a token if the password provided is valid
    """
    auth = request.authorization

    if auth is None:
        return make_response('No login credentials were provided!', 400,
                             {'WWW-Authenticate': 'Basic realm="Login Required'})

    try:
        token = authorisation_service.get_admin_token(auth.username, auth.password)
        return jsonify({'token': token})
    except authorisation_service.InvalidCredentialsError as error:
        return make_response(str(error), 401, {'WWW-Authenticate': 'Basic realm="Login Required'})
