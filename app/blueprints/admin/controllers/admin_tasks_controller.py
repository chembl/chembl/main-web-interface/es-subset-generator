"""
Blueprint for the administrative tasks of the system
"""
from flask import Blueprint, jsonify

from app.blueprints.admin.services import admin_tasks_service
from app.authorisation.decorators import admin_token_required

ADMIN_TASKS_BLUEPRINT = Blueprint('admin_tasks', __name__)


@ADMIN_TASKS_BLUEPRINT.route('/delete_test_tasks_and_indexes', methods=['GET'])
@admin_token_required
def delete_test_tasks_and_indexes():
    """
    Triggers the deletion of the tasks that were marked as testing
    :return: the result of the operation
    """
    operation_result = admin_tasks_service.delete_test_tasks_and_indexes()
    return jsonify({'operation_result': operation_result})


@ADMIN_TASKS_BLUEPRINT.route('/delete_expired_tasks', methods=['GET'])
@admin_token_required
def delete_expired_tasks():
    """
    Triggers the deletion of the expired tasks
    :return: the result of the operation
    """
    operation_result = admin_tasks_service.delete_expired_tasks()
    return jsonify({'operation_result': operation_result})
