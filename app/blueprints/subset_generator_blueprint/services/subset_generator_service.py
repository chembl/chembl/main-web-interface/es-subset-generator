"""
Subset generator service
"""
import gzip
import base64

import requests

from app import app_logging
from app.config import RUN_CONFIG
from app.es_subset_generator import subset_generator
from app.tasks.models import task_models
from app.tasks_daemon import meeseeks_launcher


class SubsetGeneratorServiceError(Exception):
    """Base class for exceptions in the subset generation service."""


class SubsetGenerationTaskNotFoundError(Exception):
    """Base class for exceptions in the subset generation service."""


def submit_subset_generation_from_ids(origin_index, items_ids, test_run=False):
    """
    Starts the process of the generation of the subset based on the ids passed as parameter
    :param origin_index: source index for the subset
    :param items_ids: ids of the items to include
    :param test_run: If true, tells me to mark the task as test so it can be easily cleaned up
    :return: a dict with the id of the task to check it's progress
    """
    fields_list = get_all_properties_list_for_index(origin_index)
    app_logging.debug(f'Going to create subset index based on {origin_index} with properties {fields_list}')
    task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list, test_run)
    meeseeks_launcher.summon_meeseeks_for_task(task_id)

    return {
        'task_id': task_id
    }


def submit_subset_generation_from_compressed_ids(origin_index, compressed_items_ids, test_run=False):
    """
    Starts the process of the generation of the subset based on the ids passed as parameter
    :param origin_index: source index for the subset
    :param compressed_items_ids: Ids compressed with the GZIP algorithm, in a base64 format
    :param test_run: If true, tells me to mark the task as test so it can be easily cleaned up
    :return: a dict with the id of the task to check it's progress
    """

    base64_bytes = base64.b64decode(compressed_items_ids)
    decompressed_bytes = gzip.decompress(base64_bytes)
    raw_ids = decompressed_bytes.decode()
    items_ids = raw_ids.split(',')

    app_logging.debug(f'{len(items_ids)} ids decompressed')

    fields_list = get_all_properties_list_for_index(origin_index)
    app_logging.debug(f'Going to create subset index based on {origin_index} with properties {fields_list}')
    task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list, test_run)
    meeseeks_launcher.summon_meeseeks_for_task(task_id)

    return {
        'task_id': task_id
    }


def submit_subset_generation_from_ids_file(origin_index, form_files, compression, test_run=False):
    """
    Starts the process of the generation of the subset based on the ids passed as parameter
    :param origin_index: source index for the subset
    :param form_files: file uploaded with the ids
    :param compression: compression used in the file
    :param test_run: If true, tells me to mark the task as test so it can be easily cleaned up
    :return: a dict with the id of the task to check it's progress
    """
    ids_file = form_files[list(form_files.keys())[0]]
    items_ids = get_items_ids_from_file(ids_file, compression)
    app_logging.debug(f'{len(items_ids)} ids read from file')
    fields_list = get_all_properties_list_for_index(origin_index)
    app_logging.debug(f'Going to create subset index based on {origin_index} with properties {fields_list}.')
    app_logging.debug(f'test_run:{test_run}')
    task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list, test_run)
    meeseeks_launcher.summon_meeseeks_for_task(task_id)

    return {
        'task_id': task_id
    }


def get_items_ids_from_file(ids_file, compression):
    """
    :param ids_file: FileStorage object with the ids
    :param compression: compression used in the file
    :return: a generator with the ids being read from the file
    """
    raw_content = ids_file.read()

    if compression == 'NO COMPRESSION':
        str_content = raw_content.decode()
        items_ids = str_content.split(',')
    elif compression == 'GZIP':
        decompressed_content = gzip.decompress(raw_content)
        str_content = decompressed_content.decode()
        items_ids = str_content.split(',')
    else:
        raise SubsetGeneratorServiceError(f'The compression format {compression} is not supported')

    return items_ids


def get_task_status(task_id):
    """
    :param task_id: the task id to check
    :return: a dict describing the status of the task
    """
    try:

        task_requested = task_models.get_task_by_id(task_id)

        return {
            'task_id': task_id,
            'task_status': str(task_requested.status),
            'progress': task_requested.progress,
            'subset_index_name': task_requested.destination_index,
            'num_items_to_process': task_requested.num_items_to_process,
            'created_at': task_requested.get_iso_created_at(),
            'started_at': task_requested.get_iso_started_at(),
            'finished_at': task_requested.get_iso_finished_at(),
            'expires_at': task_requested.get_iso_expires_at(),
            'origin_index': task_requested.origin_index,
            'num_items_with_match': task_requested.num_items_with_match,
            'num_items_with_no_match': task_requested.num_items_with_no_match
        }

    except task_models.TaskNotFoundError:

        raise SubsetGenerationTaskNotFoundError(f'The task with the id {task_id} was not found!')


def get_ids_with_no_match(task_id):
    """
    :param task_id: the task id to check
    :return: a dict  with the ids that didn't match any item in the origin index
    """
    try:

        task_requested = task_models.get_task_by_id(task_id)
        ids_with_no_match = task_requested.get_uncompressed_ids_with_no_match()

        return {
            'ids_with_no_match': ids_with_no_match,
        }

    except task_models.TaskNotFoundError:

        raise SubsetGenerationTaskNotFoundError(f'The task with the id {task_id} was not found!')


def get_all_properties_list_for_index(index_name):
    """
    :param index_name: name of the index to check
    :return: the properties list for the index indicated as parameter
    """
    properties_list_url_template = RUN_CONFIG.get('properties_list_url_template')
    properties_list_url = properties_list_url_template.format(INDEX_NAME=index_name)
    app_logging.debug(f'properties_list_url: {properties_list_url}')
    properties_list = requests.get(properties_list_url).json()['all_properties']
    app_logging.debug(f'properties_list: {properties_list}')
    app_logging.debug(f'{len(properties_list)} properties')

    return properties_list
