"""
Marshmallow schemas for validating the subset generator blueprit
"""
from marshmallow import Schema, fields


class SubmitSubsetGenerationRequest(Schema):
    """
    Class with the schema for submitting the generation of a subset
    """
    origin_index = fields.String(required=True)
    items_ids = fields.String(required=True, many=True)
    admin__test_run = fields.Boolean()


class SubmitSubsetGenerationRequestFromCompressedIDs(Schema):
    """
    Class with the schema for submitting the generation of a subset
    """
    origin_index = fields.String(required=True)
    compressed_items_ids = fields.String(required=True, many=True)
    admin__test_run = fields.Boolean()


class SubmitSubsetGenerationRequestFromFile(Schema):
    """
    Class with the schema for submitting the generation of a subset from an uploaded file
    """
    origin_index = fields.String(required=True)
    compression = fields.String(required=True)
    admin__test_run = fields.Boolean()


class SubsetGenerationStatusRequest(Schema):
    """
    Class with the schema for requesting the status of a subset generation task
    """
    task_id = fields.String(required=True)


class IDsWithNoMatchRequest(Schema):
    """
    Class with the schema for requesting the ids with no match of a task
    """
    task_id = fields.String(required=True)
