"""
    The blueprint used for submitting the generation of a subset
"""
from flask import Blueprint, jsonify, abort, request

from app.blueprints.subset_generator_blueprint.controllers import marshmallow_schemas
from app.blueprints.subset_generator_blueprint.services import subset_generator_service
from app.request_validation.decorators import validate_form_with, validate_url_params_with
from app import app_logging

SUBSET_GENERATOR_BLUEPRINT = Blueprint('es_subsets', __name__)


@SUBSET_GENERATOR_BLUEPRINT.route('/submit_subset_creation_from_ids', methods=['POST'])
@validate_form_with(marshmallow_schemas.SubmitSubsetGenerationRequest)
def submit_subset_creation_from_ids():
    """
    :return: the json response with the id of the subset creation process
    """
    form_data = request.form

    origin_index = form_data.get('origin_index')
    items_ids = form_data.get('items_ids').split(',')
    test_run = parse_boolean_param(form_data, 'admin__test_run')

    app_logging.debug(f'origin_index: {origin_index}')
    app_logging.debug(f'items_ids: {items_ids}')

    try:

        submission_response = subset_generator_service.submit_subset_generation_from_ids(origin_index, items_ids,
                                                                                         test_run)
        return jsonify(submission_response)

    except subset_generator_service.SubsetGeneratorServiceError as error:

        abort(500, msg=f'Internal server error: {str(error)}')


@SUBSET_GENERATOR_BLUEPRINT.route('/submit_subset_creation_from_compressed_ids', methods=['POST'])
@validate_form_with(marshmallow_schemas.SubmitSubsetGenerationRequestFromCompressedIDs)
def submit_subset_creation_from_compressed_ids():
    """
    :return: the json response with the id of the subset creation process
    """
    form_data = request.form

    origin_index = form_data.get('origin_index')
    compressed_items_ids = form_data.get('compressed_items_ids')
    test_run = parse_boolean_param(form_data, 'admin__test_run')

    app_logging.debug(f'origin_index: {origin_index}')
    app_logging.debug(f'compressed_items_ids: {compressed_items_ids}')

    try:

        submission_response = subset_generator_service.submit_subset_generation_from_compressed_ids(
            origin_index,
            compressed_items_ids,
            test_run)
        return jsonify(submission_response)

    except subset_generator_service.SubsetGeneratorServiceError as error:

        abort(500, msg=f'Internal server error: {str(error)}')


@SUBSET_GENERATOR_BLUEPRINT.route('/submit_subset_creation_from_file', methods=['POST'])
@validate_form_with(marshmallow_schemas.SubmitSubsetGenerationRequestFromFile)
def submit_subset_creation_from_ids_file():
    """
    :return: the json response with the id of the subset creation process
    """
    form_data = request.form
    form_files = request.files

    origin_index = form_data.get('origin_index')
    compression = form_data.get('compression')
    test_run = parse_boolean_param(form_data, 'admin__test_run')

    try:

        submission_response = subset_generator_service.submit_subset_generation_from_ids_file(origin_index, form_files,
                                                                                              compression, test_run)
        return jsonify(submission_response)

    except subset_generator_service.SubsetGeneratorServiceError as error:

        abort(500, msg=f'Internal server error: {str(error)}')


@SUBSET_GENERATOR_BLUEPRINT.route('/get_task_status/<task_id>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.SubsetGenerationStatusRequest)
def get_task_status(task_id):
    """
    :param task_id: id of the task to check
    :return: json response with status of the task
    """
    try:

        status_response = subset_generator_service.get_task_status(task_id)
        return jsonify(status_response)

    except subset_generator_service.SubsetGeneratorServiceError as error:
        abort(500, msg=f'Internal server error: {str(error)}')
    except subset_generator_service.SubsetGenerationTaskNotFoundError as error:

        abort(404, repr(error))


@SUBSET_GENERATOR_BLUEPRINT.route('/get_ids_with_no_match/<task_id>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.IDsWithNoMatchRequest)
def get_ids_with_no_match(task_id):
    """
    :param task_id: id of the task to check
    :return: json response with the ids that didn't match any item in the origin index
    """
    try:

        ids_with_no_match_response = subset_generator_service.get_ids_with_no_match(task_id)
        return jsonify(ids_with_no_match_response)

    except subset_generator_service.SubsetGeneratorServiceError as error:
        abort(500, msg=f'Internal server error: {str(error)}')
    except subset_generator_service.SubsetGenerationTaskNotFoundError as error:

        abort(404, repr(error))


def parse_boolean_param(request_params, field_name):
    """
    parses a boolean parameter in a request
    :param request_params: the dict with the request parameters
    :param field_name: name of the field to parse
    :return: True if value is taken as true, False otherwise
    """
    field_value = request_params.get(field_name, False)

    if isinstance(field_value, bool):
        return field_value

    if field_value.lower() == 'true':
        return True
    if field_value.lower() == 'false':
        return False

    return False
