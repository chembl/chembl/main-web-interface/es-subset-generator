"""
Module that implements the saving of task statistics
"""
import datetime
from datetime import timezone

from app import app_logging
from app.config import RUN_CONFIG
from app.task_statistics.es_monitoring_connection import ES_MONITORING


def save_reindexing_record(time_taken, origin_index, num_items, num_chunks, threads_multiplier, final_state):
    """
    :param time_taken: time taken to perform the task
    :param origin_index: source index from which the elements came from
    :param num_items: number of items to take from the source index
    :param num_chunks: number of chunks used
    :param threads_multiplier: threads multiplier used
    :param final_state: final state of the task
    :return: the document to save to elasticsearch
    """
    doc = get_task_statistics_document(time_taken, origin_index, num_items, num_chunks, threads_multiplier, final_state)
    task_statistics_config = RUN_CONFIG.get('task_statistics', {})
    index_name = task_statistics_config.get('task_statistics_index')
    save_record_to_elasticsearch(doc, index_name)


def get_task_statistics_document(time_taken, origin_index, num_items, num_chunks, threads_multiplier, final_state):
    """
    :param time_taken: time taken to perform the task
    :param origin_index: source index from which the elements came from
    :param num_items: number of items to take from the source index
    :param num_chunks: number of chunks used
    :param threads_multiplier: threads multiplier used
    :param final_state: final state of the task
    :return: the document to save to elasticsearch
    """

    doc = {
        "record_date": datetime.datetime.now(timezone.utc).timestamp() * 1000,
        "run_env_type": RUN_CONFIG.get('run_env'),
        "time_taken": time_taken,
        "origin_index": origin_index,
        "num_items": num_items,
        "num_chunks": num_chunks,
        "threads_multiplier": threads_multiplier,
        "final_state": final_state
    }

    return doc


# ----------------------------------------------------------------------------------------------------------------------
# Saving records to elasticsearch
# ----------------------------------------------------------------------------------------------------------------------
def save_record_to_elasticsearch(doc, index_name):
    """
    SAves the record to the monitoring elasticsearch
    :param doc: document to save
    :param index_name: name of the index where to save the document
    """
    task_statistics_config = RUN_CONFIG.get('task_statistics', {})

    dry_run = task_statistics_config.get('dry_run', False)
    es_host = task_statistics_config.get('elasticsearch', {}).get('host')

    if dry_run:
        app_logging.debug(f'Not actually sending the record to the statistics (dry run): {doc}')
    else:
        app_logging.debug(f'Sending the following record to the statistics: {doc} '
                          f'index name: {index_name} es_host: {es_host}')
        result = ES_MONITORING.index(index=index_name, body=doc, doc_type='_doc')
        app_logging.debug(f'Result {result}')
