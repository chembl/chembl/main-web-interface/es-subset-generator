"""
Module that handles the connection with the monitoring elasticsearch
"""
import elasticsearch
from app.config import RUN_CONFIG

MONITORING_CONFIG = RUN_CONFIG.get('task_statistics')
MONITORING_ELASTICSEARCH_CONFIG = MONITORING_CONFIG.get('elasticsearch')

ES_MONITORING = elasticsearch.Elasticsearch(
    [MONITORING_ELASTICSEARCH_CONFIG.get('host')],
    http_auth=(MONITORING_ELASTICSEARCH_CONFIG.get('username'),
               MONITORING_ELASTICSEARCH_CONFIG.get('password')),
    port=MONITORING_ELASTICSEARCH_CONFIG.get('port')
)
