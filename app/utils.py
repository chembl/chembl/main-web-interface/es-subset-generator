"""
Utils functions for handling the subset creation
"""
import datetime
from datetime import timezone


class ChunkSizeNonSenseError(Exception):
    """
    Error raised when the chunk size does not make sense
    """


def get_list_chunks(chunk_size, original_list):
    """
    :param chunk_size: size of chunk to obtain
    :param original_list: list to split
    :return: a generator object with the chunks of the original list with the given size
    """
    if chunk_size <= 0:
        raise ChunkSizeNonSenseError(f'A chunk size of {chunk_size} does not make sense!')

    if len(original_list) == 0:
        return []

    return prepare_list_chunks_generator(chunk_size, original_list)


def prepare_list_chunks_generator(chunk_size, original_list):
    """
    Prepares the generator to get the list chunks
    :param chunk_size: size of chunk to obta
    :param original_list: list to split
    :return: a generator object with the chunks of the original list with the given size
    """

    start_index = 0
    end_index = chunk_size
    original_list_size = len(original_list)

    yield original_list[start_index:end_index]

    while end_index < original_list_size:
        start_index = end_index
        end_index += chunk_size
        yield original_list[start_index:end_index]


def get_dict_value(dictionary, str_property, default_null_value=None):
    """
    :param dictionary: dictionary for which to get the value
    :param str_property: string path of the property, e.g  '_metadata.assay_data.assay_subcellular_fraction'
    :param default_null_value: value to return when the value in the dict is None.
     For example, it can return '' if indicated
    :return: the value of a property (separated by dots) in a dict.
    such as  '_metadata.assay_data.assay_subcellular_fraction'

    """

    prop_parts = str_property.split('.')
    current_prop = prop_parts[0]
    if len(prop_parts) > 1:
        current_obj = dictionary.get(current_prop)
        if current_obj is None:
            return default_null_value
        return get_dict_value(current_obj, '.'.join(prop_parts[1::]))

    value = dictionary.get(current_prop)
    value = default_null_value if value is None else value
    return value


def get_timestamped_text(raw_text):
    """
    :param raw_text: raw text to format
    :return: the text with a timestamp to be used to save it in logs
    """
    now = datetime.datetime.now(timezone.utc)
    stamp = now.isoformat()
    formatted_text = f'{stamp}: {raw_text}\n'
    return formatted_text
