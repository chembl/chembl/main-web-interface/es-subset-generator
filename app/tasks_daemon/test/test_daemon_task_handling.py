"""
Module to test the creation the handling of the tasks from the task daemon
"""
# pylint: disable=no-member,too-many-locals
import unittest

from app.es_subset_generator import subset_generator
from app.tasks.models import task_models
from app import create_app
from app.tasks_daemon import daemon
from app.es_connection import ES
from app.db import DB


def create_finished_task():
    """
    Creates a task that is finished
    :return: the task created
    """

    origin_index = 'chembl_molecule'
    items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
    fields_list = [
        'molecule_properties.hba',
        'molecule_synonyms',
        'molecule_properties.psa',
        '_metadata.drug.drug_data.prodrug',
        'molecule_properties.molecular_species',
        'helm_notation'
    ]

    task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)
    task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()
    task_models.set_task_state_to_finished(task_got)

    return task_got


def create_errored_task():
    """
    Creates a task that is errored
    :return: the task created
    """

    origin_index = 'chembl_molecule'
    items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212']
    fields_list = [
        'molecule_properties.hba',
        'molecule_synonyms',
        'molecule_properties.psa',
        '_metadata.drug.drug_data.prodrug',
        'molecule_properties.molecular_species',
        'helm_notation'
    ]

    task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)
    task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()
    task_models.set_task_state_to_error(task_got)

    return task_got


def create_running_task():
    """
    Creates a task that is errored
    :return: the task created
    """

    origin_index = 'chembl_molecule'
    items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148']
    fields_list = [
        'molecule_properties.hba',
        'molecule_synonyms',
        'molecule_properties.psa',
        '_metadata.drug.drug_data.prodrug',
        'molecule_properties.molecular_species',
        'helm_notation'
    ]

    task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)
    task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()
    task_models.set_task_state_to_running(task_got)

    return task_got


def create_queued_task():
    """
    Creates a task that is errored
    :return: the task created
    """

    origin_index = 'chembl_molecule'
    items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL59']
    fields_list = [
        'molecule_properties.hba',
        'molecule_synonyms',
        'molecule_properties.psa',
        '_metadata.drug.drug_data.prodrug',
        'molecule_properties.molecular_species',
        'helm_notation'
    ]

    task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)
    task_got = task_models.SubsetIndexTask.query.filter_by(id=task_id).first()
    task_models.set_task_state_to_queued(task_got)

    return task_got


class TestTaskHandling(unittest.TestCase):
    """
        Class to test the handling of tasks from the task daemon
    """

    def setUp(self):
        print('DELETING ALL TASKS')
        self.flask_app = create_app()
        self.client = self.flask_app.test_client()
        self.delete_all_test_tasks()

    def tearDown(self):
        print('DELETING ALL TASKS')
        self.delete_all_test_tasks()

    def delete_all_test_tasks(self):
        """
        Deletes all the tests tasks from the database and their respective indexes
        """
        with self.flask_app.app_context():
            subset_generator.delete_all_test_indexes()
            task_models.delete_all_tasks()

    def test_it_performs_an_indexation_task(self):
        """
        Tests that the daemon performs an indexation task correctly
        """
        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
            fields_list = [
                'molecule_properties.hba',
                'molecule_synonyms',
                'molecule_properties.psa',
                '_metadata.drug.drug_data.prodrug',
                'molecule_properties.molecular_species',
                'helm_notation'
            ]

            task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)
            popped_task = task_models.get_task_by_id(task_id)
            task_id_performed = daemon.perform_task(popped_task)

            self.assertEqual(task_id, task_id_performed,
                             msg=f'The correct task was not performed! The task {task_id} must have been performed. '
                                 f'But it seems that {task_id_performed} was performed.')

            DB.session.commit()
            DB.session.expire(popped_task)
            DB.session.refresh(popped_task)

            progress_got = popped_task.progress
            self.assertEqual(progress_got, 100, msg='The final progress must be 100!')

            status_got = popped_task.status
            self.assertEqual(status_got, task_models.TaskStatuses.FINISHED, msg='The final status must be finished!')

            destination_index = popped_task.destination_index
            results_produced = ES.search(index=destination_index)
            num_results_got = results_produced['hits']['total']['value']
            num_results_must_be = len(items_ids)
            self.assertEqual(num_results_got, num_results_must_be, msg='The index was not filled with the items!')

    def test_it_performs_an_indexation_task_when_some_items_do_not_match(self):
        """
        Tests that the daemon performs an indexation task correctly when some ids do not have match
        """
        with self.flask_app.app_context():
            origin_index = 'chembl_molecule'
            items_with_no_match = ['CHEMBL2363046']
            items_with_match = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
            items_ids = items_with_match + items_with_no_match
            fields_list = [
                'molecule_properties.hba',
                'molecule_synonyms',
                'molecule_properties.psa',
                '_metadata.drug.drug_data.prodrug',
                'molecule_properties.molecular_species',
                'helm_notation'
            ]

            task_id = subset_generator.prepare_index_and_create_reindex_task(origin_index, items_ids, fields_list)
            popped_task = task_models.get_task_by_id(task_id)
            task_id_performed = daemon.perform_task(popped_task)

            self.assertEqual(task_id, task_id_performed,
                             msg=f'The correct task was not performed! The task {task_id} must have been performed. '
                                 f'But it seems that {task_id_performed} was performed.')

            DB.session.commit()
            DB.session.expire(popped_task)
            DB.session.refresh(popped_task)

            progress_got = popped_task.progress
            self.assertEqual(progress_got, 100, msg='The final progress must be 100!')

            status_got = popped_task.status
            self.assertEqual(status_got, task_models.TaskStatuses.FINISHED, msg='The final status must be finished!')

            destination_index = popped_task.destination_index
            results_produced = ES.search(index=destination_index)
            num_results_got = results_produced['hits']['total']['value']
            num_results_must_be = len(items_with_match)
            self.assertEqual(num_results_got, num_results_must_be, msg='The index was not filled with the items!')

            items_with_no_match_got = popped_task.get_uncompressed_ids_with_no_match()
            self.assertEqual(items_with_no_match_got, items_with_no_match,
                             msg='The items with no match were not calculated correctly!')
