"""
Module that launches the status daemon in a Mr Meeseeks way
"""
import os
import shlex
import subprocess

import app.app_logging as app_logging
from app.config import RUN_CONFIG


def summon_meeseeks_for_task(task_id):
    """
    starts the job checking process. It will disappear once the job is finished or errored.
    :param task_id: id of the task to monitor
    """

    app_logging.debug(f'Going to launch Mr Meeseeks to actually perform the re-indexing task {task_id}')

    meeseeks_command = f'{RUN_CONFIG["mr_meeseeks_daemon_path"]} {task_id}'
    app_logging.debug(f'Mr Meeseeks command is: {meeseeks_command}')
    checker_env = {
        'CONFIG_FILE_PATH': os.getenv('CONFIG_FILE_PATH', 'config.yml')
    }
    app_logging.debug(f'Mr Meeseeks env is: {checker_env}')
    meeseeks_process = subprocess.Popen(shlex.split(meeseeks_command), env=checker_env)
    app_logging.debug(f'Mr Meeseeks summoned! PID is {meeseeks_process.pid}')
