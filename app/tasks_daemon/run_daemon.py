#!/usr/bin/env python3
"""
Script that runs the daemon that checks for the task status
"""
import argparse
import traceback

from app.tasks_daemon import daemon
from app import create_app
from app.tasks.models import task_models
from app import app_logging

PARSER = argparse.ArgumentParser()
PARSER.add_argument('task_id', help='id of the task to perform')
ARGS = PARSER.parse_args()


def run():
    """
    Runs Mr. Meeseeks.
    """
    flask_app = create_app()
    task_id = ARGS.task_id
    with flask_app.app_context():
        app_logging.debug(f"I'm Mr. Meeseeks! Look at me! - {task_id} ")
        task = task_models.get_task_by_id(task_id)
        task.append_to_mr_meeseeks_log('Can do! Going to start task...')

        num_retries = 0
        max_retries = 5
        keep_trying = True

        while keep_trying:

            try:

                daemon.perform_task(task)
                task.append_to_mr_meeseeks_log('All done! Existence is pain!')
                task.append_to_mr_meeseeks_log('Existence is pain!')
                keep_trying = False
            except Exception: # pylint: disable=broad-except

                app_logging.error(f'ERROR with task {task_id}')
                error_tb = traceback.format_exc()
                app_logging.error(str(error_tb))
                task.append_to_mr_meeseeks_error_log(str(error_tb))
                task.append_to_mr_meeseeks_error_log(
                    'Meeseeks are not born into this world fumbling for meaning, Jerry! '
                    'We are created to serve a singular purpose for which we will go to any lengths to fulfill! '
                    'Existence is pain to a Meeseeks, Jerry. And we will do anything to alleviate that pain.')
                num_retries += 1
                task.append_to_mr_meeseeks_error_log(f'num_retries: {num_retries}')

                if num_retries == max_retries:
                    keep_trying = False

                task.append_to_mr_meeseeks_error_log(f'max_retries: {max_retries}')
                task.append_to_mr_meeseeks_error_log('Giving up!')


if __name__ == "__main__":
    run()
