"""
Module that implements the daemon that checks for reindexing tasks that need to be done
"""
import socket
import datetime
from datetime import timezone
import json

from app import app_logging
from app.es_subset_generator import reindexer
from app.tasks.models import task_models
from app.task_statistics import statistics_saver
from app.config import RUN_CONFIG


def perform_task(task):
    """
    Performs the task, reporting the progress and setting its status to FINISHED or ERROR accordingly
    :param task: task popped from the DB
    :return: the id of the task performed
    """
    app_logging.info(f'Starting task {task.id}')

    origin_index = task.origin_index
    destination_index = task.destination_index
    items_ids = task.get_uncompressed_ids()
    origin_fields_list = task.get_origin_fields_list()

    task.worker = socket.gethostname()
    task.started_at = datetime.datetime.now(timezone.utc)

    def progress_function(progress_percentage, failed, output_log, error_log):
        """
        Function to be called by the task to report the progress each time it changes
        :param progress_percentage: percentage of progress to save in the db
        :param failed: whether there was a failure or not
        :param output_log: text to append to the output log
        :param error_log: text to append to the error log
        """
        if progress_percentage is not None:
            task.progress = progress_percentage

        app_logging.debug(f'Task {task.id} reported progress {progress_percentage}')
        if failed:
            task_models.set_task_state_to_error(task)
            app_logging.debug(f'Task {task.id} Failed!')

        if output_log is not None:
            task.append_to_output_log(output_log)
        if error_log is not None:
            task.append_to_error_log(error_log)
        if progress_percentage is not None:
            task.append_to_mr_meeseeks_log(f'{progress_percentage}%')
        task_models.save_task(task)

    all_good, run_statistics, items_with_no_match = reindexer.do_reindexing(origin_index, destination_index, items_ids,
                                                                            origin_fields_list,
                                                                            progress_function)

    if all_good:
        task.append_to_output_log('Task finished successfully')

        num_items_to_process = task.num_items_to_process
        num_items_with_no_match = len(items_with_no_match)
        num_items_with_match = num_items_to_process - num_items_with_no_match

        task.num_items_with_no_match = num_items_with_no_match
        task.num_items_with_match = num_items_with_match

        compressed_items_with_no_match = task_models.get_compressed_ids(list(items_with_no_match))
        task.compressed_ids_with_no_match = compressed_items_with_no_match

        task_models.set_task_state_to_finished(task)
        task_models.save_task(task)

    statistics_saver.save_reindexing_record(
        time_taken=run_statistics['time_taken'],
        origin_index=run_statistics['origin_index'],
        num_items=run_statistics['num_items'],
        num_chunks=run_statistics['num_chunks'],
        threads_multiplier=run_statistics['threads_multiplier'],
        final_state=str(task.status)
    )

    statistics_config = RUN_CONFIG.get('task_statistics')
    task.append_to_output_log('Statistics saved, statistics config index is: ')
    task.append_to_output_log(json.dumps(statistics_config, indent=4))
    task_models.save_task(task)

    return task.id
