"""
Module with utils functions for the functional tests
"""
import time
import datetime
from datetime import timezone
import json

import requests
from requests.auth import HTTPBasicAuth


def get_submit_url(server_base_url):
    """
    :param server_base_url: the base url of the server
    :return: the url used to submit jobs
    """
    return f'{server_base_url}/es_subsets/submit_subset_creation_from_ids'


def get_submit_from_compressed_ids_url(server_base_url):
    """
    :param server_base_url: the base url of the server
    :return: the url used to submit jobs from compressed ids
    """
    return f'{server_base_url}/es_subsets/submit_subset_creation_from_compressed_ids'


def get_submit_from_file_url(server_base_url):
    """
    :param server_base_url: the base url of the server
    :return: the url used to submit jobs
    """
    return f'{server_base_url}/es_subsets/submit_subset_creation_from_file'


def get_status_url(server_base_url, task_id):
    """
    Returns the status url to check a job given its ID
    :param server_base_url: the base url of the server
    :param task_id: task id to check
    """
    return f'{server_base_url}/es_subsets/get_task_status/{task_id}'


def get_ids_with_no_match_url(server_base_url, task_id):
    """
    :param server_base_url: the base url of the server
    :param task_id: task id to check
    :return: Returns url to check the ids with no match of a task given its ID
    """
    return f'{server_base_url}/es_subsets/get_ids_with_no_match/{task_id}'


# ----------------------------------------------------------------------------------------------------------------------
# Server Administration Helpers
# ----------------------------------------------------------------------------------------------------------------------
class ServerAdminError(Exception):
    """Base class for exceptions in the admin functions."""


def request_all_test_task_and_indices_deletion(server_base_url, admin_username, admin_password):
    """
    Requests the server to delete all test jobs. Useful to run before the start of a test.
    """

    admin_login_url = f'{server_base_url}/admin/login'
    print('admin_login_url: ', admin_login_url)
    login_request = requests.get(admin_login_url, auth=HTTPBasicAuth(admin_username, admin_password))

    if login_request.status_code != 200:
        raise ServerAdminError(f'There was a problem when logging into the administration of the system! '
                               f'(Status code: {login_request.status_code})')

    login_response = login_request.json()
    print('Token obtained')

    admin_token = login_response.get('token')
    headers = {'X-Admin-Key': admin_token}
    task_deletion_url = f'{server_base_url}/admin/delete_test_tasks_and_indexes'
    task_deletion_request = requests.get(task_deletion_url, headers=headers)
    if task_deletion_request.status_code != 200:
        raise ServerAdminError(f'There was a problem when requesting the deletion of test jobs! '
                               f'(Status code: {task_deletion_request.status_code})')
    task_deletion_response = task_deletion_request.json()

    print('task_deletion_response: ', task_deletion_response)


def assert_task_status_with_retries(status_url, status_must_be_1, status_must_be_2=None):
    """
    Asserts that the status is what it should be. It retries up to 5 times
    :param status_url: url to check status
    :param status_must_be_1: what the status should be
    :param status_must_be_2: another option for that the status must be
    :return final_status_response: final response of the status obtained
    """
    time.sleep(1)
    max_retries = 1000
    current_tries = 0
    assertion_passed = False

    while current_tries < max_retries:

        status_request = requests.get(status_url)
        print('Status request response code: ', status_request.status_code)

        status_response = status_request.json()
        task_id = status_response.get('task_id')
        task_status = status_response.get('task_status')
        progress = status_response.get('progress')

        print(f'{datetime.datetime.now(timezone.utc).isoformat()} - '
              f'job_id:{task_id} job_status: {task_status} progress: {progress}')
        print('---')

        must_not_fail = status_must_be_1 != 'ERROR' and status_must_be_2 != 'ERROR'
        if must_not_fail:
            assert task_status != 'ERROR', 'Job must have not failed!'

        assertion_passed = task_status in [status_must_be_1, status_must_be_2]
        current_tries += 1

        if assertion_passed:
            break

        time.sleep(1)

    assert assertion_passed, f'Job seems to not be {status_must_be_1}! after {current_tries} tries.'

    return status_response


def assert_subset_index_is_complete(subset_index_name, num_items_must_be, es_proxy_base_path):
    """
    Asserts that the subset index contains the amount of items that it must contain
    :param subset_index_name: subset index created with the ids.
    :param num_items_must_be: number of items that the index must have.
    :param es_proxy_base_path: base path for the es proxy
    """
    print('assert_subset_index_is_complete')
    es_proxy_data_url = f'{es_proxy_base_path}/es_data/get_es_data'
    print('es_proxy_data_url: ', es_proxy_data_url)

    es_query = {
        "size": 0,
        "sort": [],
        "track_total_hits": True
    }

    payload = {
        'index_name': subset_index_name,
        'es_query': json.dumps(es_query)
    }

    print('payload: ')
    print(payload)

    es_data_request = requests.post(es_proxy_data_url, data=payload)
    es_data_status_code = es_data_request.status_code

    es_response = es_data_request.json()

    print("index response: ")
    print(json.dumps(es_response, indent=4))

    assert es_data_status_code == 200, f'There was an error while querying the subset index! Status code was ' \
                                       f'{es_data_status_code}'

    num_items_obtained = int(es_response['es_response']['hits']['total']['value'])
    assert num_items_obtained == num_items_must_be, f'The final index has not the correct amount of items! ' \
                                                    f'I got {num_items_obtained} but must be {num_items_must_be}'


def assert_reindex_task_runs_correctly(submit_request, server_base_url, num_items_must_be, es_proxy_base_path):
    """
    Asserts that the reindex task runs correctly
    :param submit_request: request used for the submission of the task
    :param server_base_url: base url of the server
    :param num_items_must_be: number of items the subset index must finally have
    :param es_proxy_base_path: base path for the es proxy
    """

    submission_response = submit_request.json()
    print('submission_response: ', submission_response)
    task_id = submission_response.get('task_id')

    print('wait some time until it starts, it should be running...')

    status_url = get_status_url(server_base_url, task_id)
    print('status_url: ', status_url)

    final_status_response = assert_task_status_with_retries(status_url, 'FINISHED')

    print('final_status_response: ', final_status_response)
    subset_index_name = final_status_response['subset_index_name']

    assert_subset_index_is_complete(subset_index_name, num_items_must_be, es_proxy_base_path)


def assert_items_with_no_match(task_id, items_with_no_match_must_be, server_base_url):
    """
    Asserts that the items with no match registered by the task are the ones passed as parameter
    :param task_id: id of the task to check
    :param items_with_no_match_must_be: the items with no match that must be returned
    :param server_base_url: base url of the server
    """

    ids_with_no_match_url = get_ids_with_no_match_url(server_base_url, task_id)
    print('status_url: ', ids_with_no_match_url)

    ids_with_no_match_request = requests.get(ids_with_no_match_url)
    print('IDs with no match request response code: ', ids_with_no_match_request.status_code)

    ids_with_no_match_response = ids_with_no_match_request.json()
    items_with_no_match_got = ids_with_no_match_response['ids_with_no_match']
    print('items_with_no_match_got: ', items_with_no_match_got)
    print('items_with_no_match_must_be: ', items_with_no_match_must_be)

    for id_got in items_with_no_match_got:
        assert id_got in items_with_no_match_must_be, \
            f'The id {id_got} was reported as no matched but it should have matched!'
