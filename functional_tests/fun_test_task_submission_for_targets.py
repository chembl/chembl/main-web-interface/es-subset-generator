"""
Module that runs a normal simple reindexing task and expects it to run correctly.
"""
import requests

import utils


def run_test(server_base_url, admin_username, admin_password, es_proxy_base_path):
    """
    Tests that a task can run normally.
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param admin_username: admin username to use
    :param admin_password: admin password to use
    :param es_proxy_base_path: base path for the es proxy
    """

    print('------------------------------------------------------------------------------------------------')
    print('Going to test a successful reindexing for targets')
    print('------------------------------------------------------------------------------------------------')

    utils.request_all_test_task_and_indices_deletion(server_base_url, admin_username, admin_password)

    origin_index = 'chembl_target'
    items_ids = ['CHEMBL2363046', 'CHEMBL2363048', 'CHEMBL615043', 'CHEMBL2363054', 'CHEMBL5693', 'CHEMBL2366687',
                 'CHEMBL2079', 'CHEMBL6103', 'CHEMBL2364032', 'CHEMBL2364036']

    submit_url = utils.get_submit_url(server_base_url)
    payload = {
        'origin_index': origin_index,
        'items_ids': ','.join(items_ids),
        'admin__test_run': True
    }

    submit_request = requests.post(submit_url, data=payload)

    submission_status_code = submit_request.status_code
    print(f'submission_status_code: {submission_status_code}')
    print(submit_request.text)
    assert submission_status_code == 200, 'Task could not be submitted!'

    num_items_must_be = len(items_ids)
    utils.assert_reindex_task_runs_correctly(submit_request, server_base_url, num_items_must_be, es_proxy_base_path)
