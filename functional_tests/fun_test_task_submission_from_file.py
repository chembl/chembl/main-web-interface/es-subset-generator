"""
Module that runs a reindexing task from a file with the ids
"""
import requests

import utils


def run_test(server_base_url, admin_username, admin_password, es_proxy_base_path):
    """
    Tests that a task can run normally.
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param admin_username: admin username to use
    :param admin_password: admin password to use
    :param es_proxy_base_path: base path for the es proxy
    """

    print('------------------------------------------------------------------------------------------------')
    print('Going to test a submission from file')
    print('------------------------------------------------------------------------------------------------')

    utils.request_all_test_task_and_indices_deletion(server_base_url, admin_username, admin_password)

    origin_index = 'chembl_molecule'
    test_ids_file_path = 'functional_tests/data/molecule_chembl_ids.txt'
    submit_url = utils.get_submit_from_file_url(server_base_url)

    payload = {
        'origin_index': origin_index,
        'compression': 'NO COMPRESSION',
        'admin__test_run': True
    }
    files = {
        'ids_file': open(test_ids_file_path, 'rb')
    }

    submit_request = requests.post(submit_url, data=payload, files=files)

    submission_status_code = submit_request.status_code
    print(f'submission_status_code: {submission_status_code}')
    print(submit_request.text)
    assert submission_status_code == 200, 'Task could not be submitted!'

    with open(test_ids_file_path, 'rt') as ids_file:
        items_ids = ids_file.read().split(',')
        num_items_must_be = len(items_ids) - 2  # two items were removed in the new release (chembl30)

    print('num_items_must_be: ', num_items_must_be)
    utils.assert_reindex_task_runs_correctly(submit_request, server_base_url, num_items_must_be, es_proxy_base_path)
