#!/usr/bin/env python3
# pylint: disable=import-error
"""
    Script that runs the functional tests for the app
"""
import argparse

import fun_test_task_submission
import fun_test_task_submission_for_targets
import fun_test_task_submission_from_file
import fun_test_submission_from_compressed_file
import fun_test_task_submission_with_some_no_matches
import fun_test_task_submission_from_compressed_ids

PARSER = argparse.ArgumentParser()
PARSER.add_argument('server_base_path', help='server base path to run the tests against',
                    default='http://127.0.0.1:5000', nargs='?')
PARSER.add_argument('admin_username', help='Admin username',
                    default='admin', nargs='?')
PARSER.add_argument('admin_password', help='Admin password',
                    default='123456', nargs='?')
PARSER.add_argument('es_proxy_base_path', help='Base path for the es proxy', nargs='?',
                    default='https://wwwdev.ebi.ac.uk/chembl/interface_api/es_proxy')
ARGS = PARSER.parse_args()


def run():
    """
    Runs all functional tests
    """
    print(f'Running functional tests on {ARGS.server_base_path}')

    for test_module in [fun_test_task_submission, fun_test_task_submission_for_targets,
                        fun_test_task_submission_from_file,
                        fun_test_submission_from_compressed_file, fun_test_task_submission_with_some_no_matches,
                        fun_test_task_submission_from_compressed_ids]:
        test_module.run_test(ARGS.server_base_path, ARGS.admin_username, ARGS.admin_password, ARGS.es_proxy_base_path)


if __name__ == "__main__":
    run()
