"""
Module that runs a normal simple reindexing task and expects it to run correctly.
"""
import base64
import gzip

import requests

import utils


def run_test(server_base_url, admin_username, admin_password, es_proxy_base_path):
    """
    Tests that a task can run normally.
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param admin_username: admin username to use
    :param admin_password: admin password to use
    :param es_proxy_base_path: base path for the es proxy
    """

    print('------------------------------------------------------------------------------------------------')
    print('Going to test a successful reindexing from compressed ids')
    print('------------------------------------------------------------------------------------------------')

    utils.request_all_test_task_and_indices_deletion(server_base_url, admin_username, admin_password)

    origin_index = 'chembl_molecule'
    items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
    raw_test_ids = ','.join(items_ids)
    compressed_ids = gzip.compress(raw_test_ids.encode())
    base64_bytes = base64.b64encode(compressed_ids)
    base64_ids = base64_bytes.decode()

    submit_url = utils.get_submit_from_compressed_ids_url(server_base_url)
    payload = {
        'origin_index': origin_index,
        'compressed_items_ids': base64_ids,
        'admin__test_run': True
    }

    submit_request = requests.post(submit_url, data=payload)

    submission_status_code = submit_request.status_code
    print(f'submission_status_code: {submission_status_code}')
    print(submit_request.text)
    assert submission_status_code == 200, 'Task could not be submitted!'

    num_items_must_be = len(items_ids)
    utils.assert_reindex_task_runs_correctly(submit_request, server_base_url, num_items_must_be, es_proxy_base_path)
