Flask==1.1.1
pylint==2.4.2
flask-sqlalchemy==2.4.1
SQLAlchemy==1.4.43
pyyaml==5.1.2
elasticsearch>=7.0.0,<8.0.0 # Recommended by the official documentation
gunicorn==20.0.4
marshmallow==3.5.0
flask-cors==3.0.8
flask-sqlalchemy==2.4.1
requests==2.24.0
pyjwt==1.7.1
psycopg2==2.9.3
Werkzeug==1.0.1
itsdangerous==2.0.1
Jinja2==3.0.3
