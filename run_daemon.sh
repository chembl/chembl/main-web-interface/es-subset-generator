#!/usr/bin/env bash
set -x
source .venv/bin/activate ; PYTHONPATH=$PYTHONPATH:$(pwd) python3 -u app/tasks_daemon/run_daemon.py "$1"
