"""
WSGI config for the ES Subset Generator API Server.
"""
from app import create_app

FLASK_APP = create_app()
