# ==================================== BASE ====================================
ARG INSTALL_PYTHON_VERSION=${INSTALL_PYTHON_VERSION:-3.8}
FROM python:${INSTALL_PYTHON_VERSION}-slim-buster AS base
ENV CONFIG_FILE_PATH=${CONFIG_FILE_PATH:-'/etc/run_config/RUN_CONFIG.yml'}
ENV GUNICORN_CONFIG_FILE_PATH=${GUNICORN_CONFIG_FILE_PATH:-'/etc/gunicorn_config/GUNICORN_CONFIG.py'}

RUN apt-get update
RUN apt-get install -y \
    curl \
    netcat \
    iputils-ping \
    ssh \
    build-essential \
    libpq-dev \
    libmemcached-dev \
    zlib1g-dev

WORKDIR /app
COPY requirements.txt .

RUN useradd -m glados -u 4321
RUN chown -R glados:glados /app
USER glados
ENV PATH="/home/glados/.local/bin:${PATH}"

RUN python3 -m .venv ${PWD}/.venv ; source .venv/bin/activate ; pip install --user -r requirements.txt
COPY . .

FROM base AS development-server
ENTRYPOINT source .venv/bin/activate ; FLASK_APP=app flask run --host=0.0.0.0

FROM base AS production-server
ENTRYPOINT source .venv/bin/activate ; gunicorn wsgi:FLASK_APP -c ${GUNICORN_CONFIG_FILE_PATH}

